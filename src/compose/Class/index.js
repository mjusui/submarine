﻿
module.exports=class {

  template(){
    return '';
  }

  props(opt){ return opt; }
  render(opt){
    const props=this.props(opt);
    const template=this.template(opt);

    return template
      .split('\n')
    .map( line => {
      const words=line.split(/{{%|%}}/);
  
      return 1 < words.length
        ? words.map(
            (w, i) => 0 < i%2
              ? props[
                  w.replace(/^ */,'')
                   .replace(/ *$/,'')
                ]
              : w
          ).join('')
        : line;
    }).join('\n');
  }

}

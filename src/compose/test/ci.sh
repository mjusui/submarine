#!/bin/bash

dir=$(dirname $0)

regex="$dir/*.js"
[ -n "$1" ] && {
  regex="$@"
}

for js in $(ls $regex)
do
  node $js
done

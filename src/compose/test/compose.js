'use strict';
const cons=require('cons');
const fs=require('fs');
const Compose=require('compose/Class');



const render1=fs.readFileSync(
  `${__dirname}/files/render1.html`
).toString();


const render2=fs.readFileSync(
  `${__dirname}/files/render2.html`
).toString();


const template=fs.readFileSync(
  `${__dirname}/files/template.html`
).toString();
const Test=class extends Compose {
  props(val){
    return {
      x: val,
    };
  }
  template(){
    return template;
  }
}


const test1=new Test();

cons.test('Compose', (test)=>{
  test(
    test1.render('xxx')
      === render1
  );

  test(
    test1.render('yyyy')
      === render2
  )
}, 2)();

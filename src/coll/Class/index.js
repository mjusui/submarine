'use strict';


module.exports=class {
  constructor(){
    this.stack=[];
  }

  generate(opt){
    this.stack=[
      ...this.stack,
      preps => require(
        `coll/gen/${opt.type||''}`
      )(preps, opt)
    ];

    return this;
  }

  filter(opt){
    this.stack=[
      ...this.stack,
      preps => require(
        `coll/fil/${opt.type||''}`
      )(preps, opt)
    ];

    return this;
  }

  cache(opt){
    const substack=this.stack;
    this.stack=[
      require(
        `cache/${opt.type||''}`
      )(
        preps => substack.reduce(
          (a, b) => a.then(b),
          Promise.resolve(preps)
        ), opt
      )
    ];

    return this;
  }

  hosts(){
    return this.stack.reduce(
      (a, b) => a.then(b),
      Promise.resolve([])
    );
  }

}



'use strict';
const { time, test, error }=require('cons');
const coll=require('coll');

time('coll.time', (stop)=>{ 
  coll({
    type: 'gen',
    coll: 'bash',
    cmd: 'echo 10.10.1{0..2}.{1..230}',
  }, {
    type: 'fil',
    coll: 'ping',
  }).then(test('coll', (test, hosts)=>{
    stop();
    [
      '10.10.10.1',
      '10.10.10.2',
      '10.10.10.3',
      '10.10.10.4',
      '10.10.10.5',
      '10.10.10.6',
      '10.10.10.7',
      '10.10.10.11',
      '10.10.10.12',
      '10.10.10.13',
      '10.10.10.14',
      '10.10.10.15',
      '10.10.10.16',
      '10.10.10.17',
    ].forEach(addr => test(hosts.filter(
      host => host === addr
    ).length == 1));
  
  }, 14));
}, 1000 * 30, 1)();

'use strict';
const ssh=require('conn/ssh');

module.exports=(preps=[], opt)=>{
  return ssh(opt).then(res => [
    ...preps,
    ...res.stdout
      .split(/\r\n|\r|\n| /)
      .filter(r => r)
  ]);
};

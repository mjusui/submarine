'use strict';

module.exports=(preps=[], opt)=>{
  return Promise.resolve([
    ...preps,
    ...opt.array
  ]);
}

'use strict';
const sh=require('conn/sh');

module.exports=(preps=[], opt)=>{
  return sh(opt).then(res => [
    ...preps,
    ...res.stdout
      .split(/\r\n|\r|\n| /)
      .filter(r => r)
  ]);
};

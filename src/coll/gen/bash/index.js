'use strict';
const bash=require('conn/bash');

module.exports=(preps=[], opt)=>{
  return bash(opt).then(res => [
    ...preps,
    ...res.stdout
      .split(/\r\n|\r|\n| /)
      .filter(r => r)
  ]);
};

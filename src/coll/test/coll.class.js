'use strict';
const { test, error, generate }=require('cons');
const Coll=require('coll/Class');


test('coll.class', test => {

  const c1=new Coll().generate({
    type: 'sh',
    cmd: 'echo 10.10.10.10',
  }).generate({
    type: 'bash',
    cmd: 'echo 10.10.10.1{1,2}',
  }).filter({
    type: 'ping',
  }).generate({
    type: 'ssh',
    host: '10.10.10.11',
    cmd: String.raw`
      cat ${__dirname}/etc/hosts \
      |awk '{print $1}'
    `,
  }).generate({
    type: 'array',
    array: [ '10.10.10.15', '10.10.10.20' ],
  }).filter({
    type: 'ssh',
    cmd: `ls ${__dirname}/etc/hosts`,
  }).filter({
    type: 'func',
    func: hosts => hosts.filter(
      host => host.split('.')[3] % 2
    ),
  }).generate({
    type: 'func',
    func: hosts => [
      ...hosts,
      'server2',
    ],
  });

  c1.hosts().then(
    hosts => {

      [
        '10.10.10.11',
        '10.10.10.13',
        '10.10.10.15',
        'server2',
      ].forEach( addr => test(
        hosts.filter(
          host => host === addr
        ).length === 1
      ));

    }
  );

}, 4)();



'use strict';
const { test, error }=require('cons');
const coll=require('coll');

coll({
  type: 'gen',
  coll: 'sh',
  cmd: 'echo 10.10.10.10',
}, {
  type: 'gen',
  coll: 'bash',
  cmd: 'echo 10.10.10.1{1,2}',
}, {
  type: 'fil',
  coll: 'ping',
}, {
  type: 'gen',
  coll: 'ssh',
  host: '10.10.10.11',
  cmd: String.raw`
    cat ${__dirname}/etc/hosts \
    |awk '{print $1}'
  `,
}, {
  type: 'gen',
  coll: 'array',
  array: [ '10.10.10.15', '10.10.10.20' ],
}, {
  type: 'fil',
  coll: 'ssh',
  cmd: `ls ${__dirname}/etc/hosts`,
}, {
  type: 'fil',
  coll: 'func',
  func: hosts => hosts.filter(
    host => host.split('.')[3] % 2
  ),
}, {
  type: 'gen',
  coll: 'func',
  func: hosts => [
    ...hosts,
    'server2'
  ],
}).then(test('coll', (test, hosts)=>{

  [
    '10.10.10.11',
    '10.10.10.13',
    '10.10.10.15',
    'server2',
  ].forEach(addr => test(hosts.filter(
    host => host === addr
  ).length == 1));

  test(hosts.length === 4);
}, 5)); 




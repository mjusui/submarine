'use strict';
const coor=require('coor');

module.exports=(...opts)=>{
  return opts.reduce((a, b)=>{
    return a.then(
      preps => require(
        `coll/${b.type||''}/${b.coll||''}`
      )(preps, b)
    );
  }, Promise.resolve([]));
};



'use strict';
require('extended');
const sh=require('conn/sh');
const coor=require('coor');


module.exports=(preps=[], opt)=>{
  const optstr=Array.merge(
    '-c 1',
    '-W 1',
    opt.opt
  ).join(' ');

  return Promise.any(
    ...preps.map(
      prep => coor( free => sh({
        cmd: `ping ${optstr} ${prep}`,
        host: prep,
      }).finally(free), {
        name: prep,
        lim: 1,
      })
    )
  ).then(
    res => res.done.map(
      r => r.host
    )
  );
};

'use strict';
require('extended');
const ssh=require('conn/ssh');

module.exports=(preps=[], opt)=>{
  return Promise.any(
    ...preps.map(
      prep => ssh(Object.assign({}, opt, {
        host: prep,
      }))
    )
  ).then(
    res => res.done.map(
      r => r.host
    )
  );
};

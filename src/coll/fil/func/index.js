'use strict';

module.exports=(preps=[], opt)=>{
  return Promise.resolve(
    opt.func(preps)
  );
};

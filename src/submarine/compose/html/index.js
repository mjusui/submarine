'use strict';
require('extended');
const fs=require('fs');
const base64=require('util/base64');
const json=require('util/json');
const Compose=require('compose/Class');


const header=fs.readFileSync(
  `${__dirname}/templates/header.html`
).toString();

const Header=class extends Compose {
  template(){
    return header;
  }
  props(opts){
    const header_color=(
      Array.isArray(opts)
        ? opts.every( opt => opt.exams.ok )
        : opts.exams.ok
    ) ? 'rgba(0, 105, 91, 0.85)'
      : 'rgba(194, 32, 71, 0.85)';


    return {
      header_color: header_color,
    };


  }
}


const footer=fs.readFileSync(
  `${__dirname}/templates/footer.html`
).toString();

const Footer=class extends Compose {
  template(){
    return footer;
  }
  props(opts){
    const footer_color=(
      Array.isArray(opts)
        ? opts.every( opt => opt.exams.ok )
        : opts.exams.ok
    ) ? 'rgba(0, 105, 91, 0.85)'
      : 'rgba(194, 32, 71, 0.85)';


    return {
      footer_color: footer_color,
    };


  }
}



const host=fs.readFileSync(
  `${__dirname}/templates/host.html`
).toString();

const Host=class extends Compose {
  props(opts){
    const { idx, opt, files, query, stats, exams }=opts;
    const { tests, good, bad, total, ok }=exams;


    return {
      idx: idx,
      health_color: ok
        ? 'rgba(0, 105, 91, 1.0)'
        : 'rgba(194, 32, 71, 1.0)',
      target: `${opt.conn}://${opt.host||'localhost'}`,
      good_rate: `${good}/${total}`,
      query: json.unescape(
        JSON.stringify(query, null, 2)
      ),
      stats: JSON.stringify(stats, null, 2),
      tests: JSON.stringify(tests, null, 2),
    };
  }
  template(){
    return host;
  }
}


const main=fs.readFileSync(
  `${__dirname}/templates/main.html`
).toString();

const Main=class extends Compose {
  props(opts){
    const hosts=Array.isArray(opts)
      ? opts
      : [ opts ];


    return {
      date: new Date().toISOString(),
      hosts: hosts.map(
        (host,idx) => new Host()
          .render(
            Object.assign({}, host, {
               idx: idx,
            })
          )
      ).join(''),
    };
  }
  template(){
    return main;
  }
}




const base=fs.readFileSync(
  `${__dirname}/templates/base.html`
).toString();

const Base=class extends Compose {
  props(opt){
    const opts=opt.err
      ? opt.saved
      : opt;

    const error=opt.err
      ? 'error'
      : 'error_none';

    const error_log=opt.err
      ? JSON.stringify(opt.err, null, 2)
      : '';

    const error_cache=opt.err && opt.saved
      ? 'error_cache'
      : 'error_cache_none';

    const header=opts
      ? new Header()
          .render(opts)
      : '';

    const main=opts
      ? new Main()
          .render(opts)
      : '';

    const footer=opts
      ? new Footer()
          .render(opts)
      : '';

    return {
      error: error,
      error_log: error_log,
      error_cache: error_cache,
      header: header,
      main: main,
      footer: footer,
    };
  }
  template(){
    return base;
  }
}



module.exports=Base;

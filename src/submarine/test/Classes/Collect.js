'use strict';
const { Test, Test2 }=require('submarine/test/Classes/Tests');
const Submarine=require('submarine/Class');

const opts=[{
  type: 'gen',
  coll: 'sh',
  cmd: 'echo 10.10.10.10',
}, {
  type: 'gen',
  coll: 'bash',
  cmd: 'echo 10.10.10.1{1,2}',
}, {
  type: 'fil',
  coll: 'ping',
}, {
  type: 'gen',
  coll: 'ssh',
  host: '10.10.10.11',
  cmd: String.raw`
    cat ${__dirname}/etc/hosts \
    |awk '{print $1}'
  `,
}, {
  type: 'gen',
  coll: 'array',
  array: [ '10.10.10.15', '10.10.10.20' ],
}, {
  type: 'fil',
  coll: 'ssh',
  cmd: `ls ${__dirname}/etc/hosts`,
}, {
  type: 'fil',
  coll: 'func',
  func: hosts => hosts.filter(
    host => host.split('.')[3] % 2
  ),
}];


const Tests=Submarine.collect(
  host => new Test({
    conn: 'ssh',
    host: host,
  }), ...opts
);

const Tests2=Submarine.collect(
  host => new Test2({
    conn: 'ssh',
    host: host,
  }), ...opts
);

const Hosts=Submarine.hosts(
  host => new Test({
    conn: 'ssh',
    host: host,
  }),
  '10.10.10.11',
  '10.10.10.13',
  '10.10.10.15'
);



module.exports={
  Tests: Tests,
  Tests2: Tests2,
  Hosts: Hosts,
};

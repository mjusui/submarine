'use strict';
const Submarine=require('submarine/Class');


const Increment=class extends Submarine {
  constructor(opt){
    super(opt);
    this.cnt=0;
  }
  fetch(){
    return [{
      a: 'files/a.txt'
    }.map(
      rel => `${__dirname}/${rel}`
    )];
  }
  query(r){
    return {
      a: `echo ${this.cnt++}`,
      file: `test -r ${r.files[0].a}`,
    };
  }
}
const Increments=Submarine.hosts(
  conn => new Increment({
    conn: conn,
  }),

  'sh',
  'bash',
  'sh'
);

const Collaborate=class extends Submarine {

  query(r){
    return {
      collabs_length: String.raw`
        echo ${r.collabs.length}
      `,

      collabs_stats_a: String.raw`
        echo ${ r.collabs.map(
          collab => collab.stats.a
        ).join(' ')}
      `,

    }
  }

  test(r){
    return {
      collabs_length_same:
        r.stats.collabs_length*1 === r.collabs.length,

      collabs_stats_a_same:
        r.stats.collabs_stats_a === r.collabs.map(
          collab => collab.stats.a
        ).join(' '),
    };
  }
}

const Collaborates=Submarine.hosts(
  host => new Collaborate({
    conn: 'ssh',
    host: host,
  }),

  '10.10.10.14',
  '10.10.10.15',
  '10.10.10.16'
);


const CollaborateChain=Submarine.chain(
  Collaborate,
  Collaborates
);

module.exports={
  Increment: Increment,
  Increments: Increments,
  Collaborate: Collaborate,
  Collaborates: Collaborates,
  CollaborateChain: CollaborateChain,
};





'use strict';
const Submarine=require('submarine/Class');


const Test=class extends Submarine {
  testThisFuncIsCallable(){
    return 'callable';
  }
  fetch(){
    console.assert(
      this.testThisFuncIsCallable()
      == 'callable'
    , 'this.func is not callable');

    return [{
      conn: 'ssh',
      host: '10.10.10.17',
      files: {
        a: 'files/a.txt',
        b: 'files/b.txt',
        dir: 'files/dir',
      }.map(
        rel => `${__dirname}/${rel}`
      ),
    }];
  }
  query(r){
    const files=r.files[0];

    console.assert(
      this.testThisFuncIsCallable()
      == 'callable'
    , 'this.func is not callable');

    return {
      a: `cat ${files.a}`,
      b: `cat ${files.b}`,
      c: `cat ${files.dir}/c.txt`,
      d: 'echo not formatted',
    };
  }
  
  format(stats){
    console.assert(
      this.testThisFuncIsCallable()
      == 'callable'
    , 'this.func is not callable');

    return {
      a: stats.a,
      b: stats.b,
      c: stats.c,
    };
  }

  test(r){
    const stats=r.stats;
    const files=r.files[0];

    console.assert(
      this.testThisFuncIsCallable()
      == 'callable'
    , 'this.func is not callable');

    return {
      a: stats.a === 'a',
      b: stats.b === 'b',
      c: stats.c === 'c',
    };
  }

  batch(r){
    const stats=r.stats;
    const files=r.files[0];

    console.assert(
      this.testThisFuncIsCallable()
      == 'callable'
    , 'this.func is not callable');

    return 'echo batch';
  }
}

const Test2=class extends Test {
  test(r){
    const stats=r.stats;
    const files=r.files[0];

    return {
      a: stats.a === 'a',
      b: stats.b === 'b',
      c: stats.c === 'c',
      dummy: false,
    };
  }

  command(){
    return 'echo command';
  }
}


const MultilinedQuery=class extends Submarine {
  query(){
    return {
      non_sense: String.raw`
        echo a
        echo b

        echo c \
        |grep c
      `,
    };
  }
  test(){
    return {
      non_sence: true,
    };
  }
}

const Ctrl=class extends Test {
  format(stats){
    return Object.assign({
      time: new Date()
        .getTime(),
    }, super.format(stats))
  }
}
module.exports={
  Test: Test,
  Test2: Test2,
  MultilinedQuery: MultilinedQuery,
  Ctrl: Ctrl,
};

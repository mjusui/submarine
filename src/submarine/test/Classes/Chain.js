'use strict';
const { Test, Test2 }=require('submarine/test/Classes/Tests');
const Submarine=require('submarine/Class');


const Pass=class extends Submarine {
  query(){
    return {
      pass: 'echo pass',
    };
  }
  test(r){
    const stats=r.stats;

    return {
      pass: stats.pass === 'pass',
    };
  }
  batch(r){
    const stats=r.stats;

    return `echo ${stats.pass}`;
  }
}


const Stop=class extends Pass {
  test(){
    return {
      pass: false,
    };
  }
  command(r){
    const stats=r.stats;

    return `echo ${stats.pass}`;
  }

}

const MustPass=Submarine.chain(
  Pass,
  Pass,
  Pass
);

const MustStop=Submarine.chain(
  Pass,
  Stop,
  Pass,
  Pass
);


const CheckFiles=Submarine.chain(
  Test,
  Submarine.hosts(
    host => new Test({
      conn: 'ssh',
      host: host,
    }),

    '10.10.10.12',
    '10.10.10.13'
  ),
  Test2,
  Test
);

 


module.exports={
  MustPass: MustPass,
  MustStop: MustStop,
  CheckFiles: CheckFiles,
};

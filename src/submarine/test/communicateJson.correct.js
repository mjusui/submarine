'use strict';
const http=require('http');
const { arrows }=require('cons');
const { Test2 }=require('submarine/test/Classes/Tests');

const test2=new Test2({
  conn: 'ssh',
  host: '10.10.10.12',
});

const server=test2.communicateJson({
  pathname: '/x/y/z',
});


arrows('PUT').arch(
  meth => new Promise((resl, rejc)=>{
    const req=http.request({
      method: meth,
      protocol: 'http:',
      host: 'localhost',
      port: 30117,
      path: '/x/y/z',
    }, (res)=>{
      let body='';
  
      res.on(
        'data',
        data => body+=data
      );
  
      res.on(
        'end',
        none => resl(
          JSON.parse(body)
        )
      );
    });

    req.on(
      'error',
      err =>  rejc(err)
    );
    req.end();
  })
).once('Submarine.communicate.command', (test, r)=>{
  const { opt, files, query, stats, exams, exec }=r;

  test(opt.conn === 'ssh');
  test(opt.host === '10.10.10.12');

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);

  const { tests, good, bad, total, ok }=exams;

  test(tests.a == true);
  test(tests.b == true);
  test(tests.c == true);
  test(tests.dummy == false);

  test(good == 3);
  test(bad == 1);
  test(total == 4);
  test(ok == false);


  const { cmd, conn, host,
    err, stdout, stderr, 
    stdouts, stderrs }=exec;

  test(cmd === 'echo command');
  test(conn === 'ssh');
  test(host === '10.10.10.12');
  test(err == null);
  test(stdout === 'command');
  test(stdouts.length == 1);
  test(stdouts[0] == 'command');
  test(stderr === '');
  test(stderrs.length == 1);
  test(stderrs[0] == '');

  test2.close();
  server.close();
}, 32);





'use strict';
const { arrows }=require('cons');
const { Test, Test2 }=require('submarine/test/Classes/Tests');

const Submarine=require('submarine/Class');


arrows('sh', 'bash', 'ssh').arch((conn,idx)=>{
  const test=new Test({
    conn: conn,
    host: conn === 'ssh'
      ? '10.10.10.11'
      : undefined
  });

  return test.correct().then(
    ret => test.close().then(
      none => ret
    )
  );

}).thrice('Submarine.command.correct', (test, r)=>{
  const { opt, files, query, stats, exams }=r;

  test(
       opt.conn === 'sh'
    || opt.conn === 'bash'
    || (
        opt.conn === 'ssh'
        && opt.host === '10.10.10.11'
    )
  );

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);

  const { tests, good, bad, total, ok }=exams;

  test(tests.a == true);
  test(tests.b == true);
  test(tests.c == true);
  test(tests.d == undefined);

  test(good == 3);
  test(bad == 0);
  test(total == 3);
  test(ok == true);
}, 63);




arrows('sh', 'bash', 'ssh').arch((conn,idx)=>{
  const test=new Test2({
    conn: conn,
    host: conn === 'ssh'
      ? '10.10.10.11'
      : undefined,
    close: 'x',
  });

  return test.correct(r=>r).then(
    ret => test.close().then(
      none => ret
    )
  );

}).thrice('Submarine.command.correct.run', (test, r)=>{
  const { opt, files, query, stats, exams, exec }=r;

  test(
       opt.conn === 'sh'
    || opt.conn === 'bash'
    || (
        opt.conn === 'ssh'
        && opt.host === '10.10.10.11'
    )
  );

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);

  const { tests, good, bad, total, ok }=exams;

  test(tests.a == true);
  test(tests.b == true);
  test(tests.c == true);
  test(tests.dummy == false);

  test(good == 3);
  test(bad == 1);
  test(total == 4);
  test(ok == false);


  const { cmd, conn, host,
    err, stdout, stderr, 
    stdouts, stderrs }=exec;

  test(cmd === 'echo command');
  test(
    conn === 'sh'
    || conn === 'bash'
    || conn === 'ssh'
  );
  test(
    conn === 'ssh'
      ? host === '10.10.10.11'
      : host == undefined
  );
  test(err == null);
  test(stdout === 'command');
  test(stdouts.length == 1);
  test(stdouts[0] == 'command');
  test(stderr === '');
  test(stderrs.length == 1);
  test(stderrs[0] == '');
}, 93);





'use strict';
const { arrows }=require('cons');
const { Tests, Hosts }=require('submarine/test/Classes/Collect');

const tests=new Tests();
const hosts=new Hosts();

arrows(tests, hosts).arch(
  tests => tests.current().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).twice('Submarine.collect.query', (test, results)=>{

  results.forEach( r => {
    const { opt, files, query, stats }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);

  });

}, 13 * 6);



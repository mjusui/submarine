'use strict';
const { arrows }=require('cons');
const {
  Increment,
  Increments,
  Collaborate,
  Collaborates,
  CollaborateChain,
}=require('submarine/test/Classes/Collaborate');

const increm1=new Increment({
  conn: 'sh',
});
const increm2=new Increment({
  conn: 'sh',
});
const increm3=new Increment({
  conn: 'sh',
});

const increms1=new Increments();
const increms2=new Increments();
const increms3=new Increments();

const collaborate=new Collaborate({
  conn: 'ssh',
  host: '10.10.10.11',
}).collaborate(
  increm1,
  increm1,
  increm1,
  increms1,
  increms1
);

const collaborates=new Collaborates()
  .collaborate(
  increm2,
  increm2,
  increm2,
  increms2,
  increms2
);

const collaboratechain=new CollaborateChain({
  conn: 'ssh',
  host: '10.10.10.12',
}).collaborate(
  increm3,
  increm3,
  increm3,
  increms3,
  increms3
);

arrows(
  collaborate,
  collaborates,
  collaboratechain
).arch(
  c => c.check().finally(
    none => c.close()
  )
).once('Submarine.collaborate', (test, r)=>{
  test(r.collabs.length == 9);

  test(r.collabs[0].stats.a === '0');
  test(r.collabs[1].stats.a === '1');
  test(r.collabs[2].stats.a === '2');
  test(r.collabs[3].stats.a === '0');
  test(r.collabs[4].stats.a === '0');
  test(r.collabs[5].stats.a === '0');
  test(r.collabs[6].stats.a === '1');
  test(r.collabs[7].stats.a === '1');
  test(r.collabs[8].stats.a === '1');

  const { tests, good, bad, total, ok }=r.exams;

  test(tests.collabs_length_same == true);
  test(tests.collabs_stats_a_same === true);

  test(good == 2);
  test(bad == 0);
  test(total == 2);
  test(ok == true);

}, 16).once('Submarine.collaborate.collect', (test, r)=>{
  r.forEach( r => {
    test(r.collabs.length == 9);
    
    test(r.collabs[0].stats.a === '0');
    test(r.collabs[1].stats.a === '1');
    test(r.collabs[2].stats.a === '2');
    test(r.collabs[3].stats.a === '0');
    test(r.collabs[4].stats.a === '0');
    test(r.collabs[5].stats.a === '0');
    test(r.collabs[6].stats.a === '1');
    test(r.collabs[7].stats.a === '1');
    test(r.collabs[8].stats.a === '1');
  
    const { tests, good, bad, total, ok }=r.exams;
  
    test(tests.collabs_length_same == true);
    test(tests.collabs_stats_a_same === true);
  
    test(good == 2);
    test(bad == 0);
    test(total == 2);
    test(ok == true);
  });
}, 48).once('Submarine.collaborate.chain', (test, r)=>{

  r.forEach( r => {
    test(r.collabs.length == 9);
    
    test(r.collabs[0].stats.a === '0');
    test(r.collabs[1].stats.a === '1');
    test(r.collabs[2].stats.a === '2');
    test(r.collabs[3].stats.a === '0');
    test(r.collabs[4].stats.a === '0');
    test(r.collabs[5].stats.a === '0');
    test(r.collabs[6].stats.a === '1');
    test(r.collabs[7].stats.a === '1');
    test(r.collabs[8].stats.a === '1');
  
    const { tests, good, bad, total, ok }=r.exams;
  
    test(tests.collabs_length_same == true);
    test(tests.collabs_stats_a_same === true);
  
    test(good == 2);
    test(bad == 0);
    test(total == 2);
    test(ok == true);
  });
}, 64);



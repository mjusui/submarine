'use strict';
const { arrows }=require('cons');
const { Test }=require('submarine/test/Classes/Tests');

const TestFormat=class extends Test {
  format(stats){
    return {
      d: 'd',
      e: 'e',
    };
  }
}

arrows('sh', 'bash', 'ssh').arch( conn => {
  const test=new TestFormat({
    conn: conn,
    host: conn === 'ssh'
      ? '10.10.10.11'
      : undefined
  })

  return test.current().then(
    ret => test.close().then(
      dummy => ret.stats
    )
  );
}).thrice('Submarine.query', (test, stats)=>{
  const { a, b, c, d, e, f }=stats;
  test(a == undefined);
  test(b == undefined);
  test(c == undefined);
  test(d === 'd');
  test(e === 'e');
  test(f == undefined);
}, 18);




'use strict';
const { arrows, generate }=require('cons');
const {
  MustPass,
  MustStop,
  CheckFiles,
}=require('submarine/test/Classes/Chain');

const mustpass=new MustPass({
  conn: 'ssh',
  host: '10.10.10.11',
});

const muststop=new MustStop({
  conn: 'ssh',
  host: '10.10.10.11',
})

const checkfiles=new CheckFiles({
  conn: 'ssh',
  host: '10.10.10.11',
});

arrows(
  mustpass,
  muststop,
  checkfiles
).arch(
  tests => tests.current().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.chain.query.pass', (test, results)=>{
  results.forEach( r =>{
    const { opt, files, query ,stats, exams, }=r;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    files.forEach( files => {
      test(Object.keys(files).length === 0);
    });

    test(query.pass === 'echo pass');
    test(stats.pass === 'pass');
    test(exams == undefined);
  });
}, 18).once('Submarine.chain.query.stop', (test, results)=>{
  [
    results[0],
    results[1],
  ].forEach( r => {
    const { opt, files, query, stats, exams, }=r;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    test(files.length == 1);
    test(query.pass === 'echo pass');
    test(stats.pass === 'pass');
    test(exams == undefined);
  });

  [
    results[2],
  ].forEach( r => {
    const { opt, files, query, stats, exams, }=r;
    const { tests, good, bad, total, ok, }=exams;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    test(files.length == 1);
    test(query.pass === 'echo pass');
    test(stats.pass === 'pass');
    test(tests.pass == false);

    test(good == 0);
    test(bad == 1);
    test(total == 1);
    test(ok == false);
  });

}, 22).once('Submarine.chain.query.files', (test, results)=>{
  [
    results[0],
    results[1],
    results[2],
    results[3],
  ].forEach( (r, i) => {
    const { opt, files, query, stats, exams }=r;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.1' + (1 + (i % 3)) );

    test(files.length == 1);
    files.forEach(
      files => test(Object.keys(files).length == 3)
    );

    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');

    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);

    test(exams == undefined);
  });

  [
    results[4],
  ].forEach( r => {
    const { opt, files, query, stats, exams, }=r;
    const { tests, good, bad, total, ok, }=exams;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    test(files.length == 1);
    files.forEach(
      files => test(Object.keys(files).length === 3)
    );

    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');

    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);

    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.dummy == false);

    test(good == 3);
    test(bad == 1);
    test(total == 4);
    test(ok == false);
  });


  /*results.forEach( generate(
    stats => {
      test(stats.a === 'a');
      test(stats.b === 'b');
      test(stats.c === 'c');
    },
    stats => {
      test(stats.a === 'a');
      test(stats.b === 'b');
      test(stats.c === 'c');
    },
    stats => {
      test(stats.a === 'a');
      test(stats.b === 'b');
      test(stats.c === 'c');
    },
    stats => {
      test(stats.a === 'a');
      test(stats.b === 'b');
      test(stats.c === 'c');
    },
    exams => {
      const { stats, tests,
        good, bad, total, ok }=exams;

      test(stats.a === 'a');
      test(stats.b === 'b');
      test(stats.c === 'c');

      test(tests.a == true);
      test(tests.b == true);
      test(tests.c == true);
      test(tests.dummy == false);

      test(good === 3);
      test(bad === 1);
      test(total === 4);
      test(ok == false); 
    }
  ) );*/

}, 13*4 + 20);




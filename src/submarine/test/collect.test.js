'use strict';
const { arrows }=require('cons');
const { Tests }=require('submarine/test/Classes/Collect');

const tests=new Tests();

arrows(tests).arch(
  tests => tests.check().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.collect.test', (test, results)=>{

  results.forEach( r => {
    const { opt, files, query, stats, exams }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);
  
    const { tests, good, bad, total, ok }=exams;
  
    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.d == undefined);
  
    test(good == 3);
    test(bad == 0);
    test(total == 3);
    test(ok == true);

  });


}, 21 * 3);





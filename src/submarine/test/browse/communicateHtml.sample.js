'use strict';
const { MultilinedQuery }=require('submarine/test/Classes/Tests');
const { Hosts, Tests2 }=require('submarine/test/Classes/Collect');
const { Ctrl }=require('submarine/test/Classes/Tests');
const Submarine=require('submarine/Class');


const multilined=new MultilinedQuery({
  conn: 'bash',
});
multilined.communicateHtml({
  pathname: '/multilined.html',
  port: 30118,
});


const test1=new Hosts();

const server1=test1.communicateHtml({
  pathname: '/status.html',
  port: 30118,
});



const test2=new Tests2();

const server2=test2.communicateHtml({
  pathname: '/failed.html',
  port: 30118,
});


let cnt=0;
const CacheError=class extends Submarine {
  query(){
    return {
      once: String.raw`
        exit $(( ${cnt++} / 6 ))
      `,
    }
  }
  test(stats){
    return {
      dummy: true,
    };
  }
}

const CacheErrors=Submarine.hosts(
  host => new CacheError({
    conn: 'ssh',
    host: host,
  }),

  '10.10.10.11',
  '10.10.10.12',
  '10.10.10.13',
  '10.10.10.14'
);

const cache_errors=new CacheErrors();

const server3=cache_errors.communicateHtml({
  pathname: '/cache-error.html',
  port: 30118,
});



const Error=class extends Submarine {
  query(){
    return {
      err: 'exit 1',
    }
  }
}
const error=new Error({
  conn: 'sh'
});
const server4=error.communicateHtml({
  pathname: '/error.html',
  port: 30118,
});


const ctrl=new Ctrl({
  conn: 'bash',
}).control({
  interval: 5100,
});
const server=ctrl.communicateHtml({
  pathname: '/ctrl.html',
  port: 30118,
});


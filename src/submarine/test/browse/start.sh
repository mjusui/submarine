#!/bin/bash

dir=$(dirname $0)
  mkdir -p $dir/log



for js in \
  communicateHtml.sample.js
do
  node $dir/$js \
    > $dir/log/stdout.log \
    2> $dir/log/stderr.log \
  &
done


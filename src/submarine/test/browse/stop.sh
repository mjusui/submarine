#!/bin/bash

dir=$(dirname $0)
  mkdir -p $dir/log



for js in \
  communicateHtml.sample.js
do
  for pid in $(
    ps aux \
    |grep -v grep \
    |grep $js \
    |awk '{print $2}'
  );do
    echo kill $pid
    kill $pid \
      && rm -rf /tmp/submarine-*
  done
done




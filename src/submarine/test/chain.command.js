'use strict';
const { arrows, generate }=require('cons');
const {
  MustPass,
  MustStop,
  CheckFiles,
}=require('submarine/test/Classes/Chain');

const mustpass=new MustPass({
  conn: 'ssh',
  host: '10.10.10.11',
});

const muststop=new MustStop({
  conn: 'ssh',
  host: '10.10.10.11',
})



arrows(
  mustpass,
  muststop,
).arch(
  tests => tests.correct().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.chain.command.correct.pass', (test, results)=>{
  results.forEach(
    r => {
      const { opt, files, query, stats, exams, }=r;
      const { tests, good, bad, total, ok, }=exams;

      test(opt.conn === 'ssh');
      test(opt.host === '10.10.10.11');

      test(files.length == 1);
      test(query.pass === 'echo pass');
      test(stats.pass === 'pass');
      test(tests.pass == true);

      test(good == 1);
      test(bad == 0);
      test(total == 1);
      test(ok == true);
    }
  );
}, 30).once('Submarine.chain.command.correct.stop', (test, results)=>{
  results.forEach( generate(
    r => {
      const { opt, files, query, stats, exams, }=r;
      const { tests, good, bad, total, ok, }=exams;

      test(opt.conn === 'ssh');
      test(opt.host === '10.10.10.11');

      test(files.length == 1);
      test(query.pass === 'echo pass');
      test(stats.pass === 'pass');
      test(tests.pass == true);

      test(good == 1);
      test(bad == 0);
      test(total == 1);
      test(ok == true);
    },
    r => {
      const { opt, files, query, stats, exams, exec }=r;
      const { tests, good, bad, total, ok, }=exams;

      test(opt.conn === 'ssh');
      test(opt.host === '10.10.10.11');

      test(files.length == 1);
      test(query.pass === 'echo pass');
      test(stats.pass === 'pass');
      test(tests.pass == false);

      test(good == 0);
      test(bad == 1);
      test(total == 1);
      test(ok == false);


      const { conn, host, cmd,
        err, stdout, stderr,
        stdouts, stderrs }=exec;

      test(conn === 'ssh');
      test(host === '10.10.10.11');
      test(cmd === 'echo pass');
      test(err == null);
      test(stdout === 'pass');
      test(stderr === '');
      test(stdouts.length == 1);
      test(stdouts[0] == 'pass');
      test(stderrs.length == 1);
      test(stderrs[0] == '');
    },
    r => {
      const { opt, files, query, stats, exams, exec }=r;
      const { tests, good, bad, total, ok, }=exams;

      test(opt.conn === 'ssh');
      test(opt.host === '10.10.10.11');

      test(files.length == 1);
      test(query.pass === 'echo pass');
      test(stats.pass === 'pass');
      test(tests.pass == false);

      test(good == 0);
      test(bad == 1);
      test(total == 1);
      test(ok == false);
    }
  ) );
}, 40);



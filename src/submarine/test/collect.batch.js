'use strict';
const { arrows, generate }=require('cons');
const { Tests, Tests2 }=require('submarine/test/Classes/Collect');


arrows(
  new Tests2(),
  new Tests(),
).arch(
  tests => tests.call().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.collect.batch', (test, results)=>{

  results.forEach( r => {
    const { opt, files, query, stats, exams }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);
  
    const { tests, good, bad, total, ok }=exams;
  
    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.dummy == false);
  
    test(good == 3);
    test(bad == 1);
    test(total == 4);
    test(ok == false);

  })


}, 63).once('Submarine.collect.batch.run', (test, results)=>{

  results.forEach( r => {
    const { opt, files, query, stats, exams, exec }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);
  
    const { tests, good, bad, total, ok }=exams;
  
    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.d == undefined);
  
    test(good == 3);
    test(bad == 0);
    test(total == 3);
    test(ok == true);
  
  
    const { cmd, conn, host,
      err, stdout, stderr, 
      stdouts, stderrs }=exec;
  
    test(cmd === 'echo batch');
    test(
      conn === 'sh'
      || conn === 'bash'
      || (
        conn === 'ssh'
        && typeof host === 'string'
      )
    );
    test(err == null);
    test(stdout === 'batch');
    test(stdouts.length == 1);
    test(stdouts[0] == 'batch');
    test(stderr === '');
    test(stderrs.length == 1);
    test(stderrs[0] == '');

  })


}, 90);




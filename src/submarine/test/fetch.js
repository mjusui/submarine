'use strict';
const { arrows }=require('cons');
const { Test }=require('submarine/test/Classes/Tests');

const test=new Test({
  conn: 'ssh',
  host: '10.10.10.11',
});

arrows(test, test).arch(
  test => test.current().then(
    ret => test.close().then(
      dummy => ret.stats
    )
  ).catch(ret => ret)
).once('Submarine.fetch', (test, stats)=>{
  const { a, b, c, d }=stats;
  test(a === 'a');
  test(b === 'b');
  test(c === 'c');
  test(d == undefined);
}, 4).once('Submarine.fetch.error', (test, result)=>{
  test( !(result.err == null) );
}, 1);




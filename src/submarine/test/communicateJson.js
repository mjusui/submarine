'use strict';
const http=require('http');
const { arrows }=require('cons');
const { Test }=require('submarine/test/Classes/Tests');

const test1=new Test({
  conn: 'ssh',
  host: '10.10.10.12',
});

const server=test1.communicateJson({
  pathname: '/x/y/z',
  format: r => Object.assign(r, {
    formatted: true,
  }),
});


arrows('GET', 'PUT', 'POST').arch(
  meth => new Promise((resl, rejc)=>{
    const req=http.request({
      method: meth,
      protocol: 'http:',
      host: 'localhost',
      port: 30117,
      path: '/x/y/z',
    }, (res)=>{
      console.assert(
        res.headers['content-type'] === 'application/json',
        'content-type is not application/json'
      );

      let body='';
  
      res.on(
        'data',
        data => body+=data
      );
  
      res.on(
        'end',
        none => {
          const json=JSON.parse(body);

          console.assert(
            meth === 'GET'
              ? json.formatted == true
              : meth === 'PUT'
                || meth === 'POST',
            'response not filtered'
          );

          resl(json);
        }
      );
    });

    req.on(
      'error',
      err =>  rejc(err)
    );
    req.end();
  })
).twice('Submarine.communicate.test', (test, r)=>{
  const { opt, files, query, stats, exams }=r;

  test(opt.conn === 'ssh');
  test(opt.host === '10.10.10.12');

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);

  const { tests, good, bad, total, ok }=exams;

  test(tests.a == true);
  test(tests.b == true);
  test(tests.c == true);
  test(tests.d == undefined);

  test(good == 3);
  test(bad == 0);
  test(total == 3);
  test(ok == true);

}, 44).once('Submarine.communicate.batch', (test, r)=>{
  const { opt, files, query, stats, exams, exec }=r;

  test(opt.conn === 'ssh');
  test(opt.host === '10.10.10.12');

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);

  const { tests, good, bad, total, ok }=exams;

  test(tests.a == true);
  test(tests.b == true);
  test(tests.c == true);
  test(tests.d == undefined);

  test(good == 3);
  test(bad == 0);
  test(total == 3);
  test(ok == true);


  const { cmd, conn, host,
    err, stdout, stderr, 
    stdouts, stderrs }=exec;

  test(cmd === 'echo batch');
  test(conn === 'ssh');
  test(host === '10.10.10.12');
  test(err == null);
  test(stdout === 'batch');
  test(stdouts.length == 1);
  test(stdouts[0] == 'batch');
  test(stderr === '');
  test(stderrs.length == 1);
  test(stderrs[0] == '');


  test1.close();
  server.close();
}, 32);




'use strict';
const { arrows }=require('cons');
const { Test }=require('submarine/test/Classes/Tests');


arrows('sh', 'bash', 'ssh').arch( conn => {
  const test=new Test({
    conn: conn,
    host: conn === 'ssh'
      ? '10.10.10.11'
      : undefined
  })

  return test.current().then(
    ret => test.close().then(
      dummy => ret
    )
  );
}).thrice('Submarine.query', (test, r)=>{
  const { opt, files, query, stats }=r;

  test(
       opt.conn === 'sh'
    || opt.conn === 'bash'
    || (
        opt.conn === 'ssh'
        && opt.host === '10.10.10.11'
    )
  );

  files.forEach( f => {
    test(typeof f.a === 'string');
    test(typeof f.b === 'string');
    test(typeof f.dir === 'string');
    test(f.c == undefined);
  });

  test(typeof query.a === 'string');
  test(typeof query.b === 'string');
  test(typeof query.c === 'string');
  test(query.d === 'echo not formatted');

  test(stats.a === 'a');
  test(stats.b === 'b');
  test(stats.c === 'c');
  test(stats.d == undefined);
}, 39);



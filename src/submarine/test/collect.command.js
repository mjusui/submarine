'use strict';
const { arrows, generate }=require('cons');
const { Tests, Tests2 }=require('submarine/test/Classes/Collect');


arrows(
  new Tests(),
  new Tests2(),
).arch(
  tests => tests.correct().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.collect.command', (test, results)=>{
  results.forEach( r => {
    const { opt, files, query, stats, exams }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);
  
    const { tests, good, bad, total, ok }=exams;
  
    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.d == undefined);
  
    test(good == 3);
    test(bad == 0);
    test(total == 3);
    test(ok == true);

  });

}, 63).once('Submarine.collect.command.run', (test, results)=>{

  results.forEach( r => {
    const { opt, files, query, stats, exams, exec }=r;
  
    test(
         opt.conn === 'sh'
      || opt.conn === 'bash'
      || (
          opt.conn === 'ssh'
          && typeof opt.host === 'string'
      )
    );
  
    files.forEach( f => {
      test(typeof f.a === 'string');
      test(typeof f.b === 'string');
      test(typeof f.dir === 'string');
      test(f.c == undefined);
    });
  
    test(typeof query.a === 'string');
    test(typeof query.b === 'string');
    test(typeof query.c === 'string');
    test(query.d === 'echo not formatted');
  
    test(stats.a === 'a');
    test(stats.b === 'b');
    test(stats.c === 'c');
    test(stats.d == undefined);
  
    const { tests, good, bad, total, ok }=exams;
  
    test(tests.a == true);
    test(tests.b == true);
    test(tests.c == true);
    test(tests.dummy == false);
  
    test(good == 3);
    test(bad == 1);
    test(total == 4);
    test(ok == false);
  
  
    const { cmd, conn, host,
      err, stdout, stderr, 
      stdouts, stderrs }=exec;
  
    test(cmd === 'echo command');
    test(
      conn === 'sh'
      || conn === 'bash'
      || (
        conn === 'ssh'
        && typeof host === 'string'
      )
    );

    test(err == null);
    test(stdout === 'command');
    test(stdouts.length == 1);
    test(stdouts[0] == 'command');
    test(stderr === '');
    test(stderrs.length == 1);
    test(stderrs[0] == '');

  });



  /*results.forEach((results)=>{
    const { cmd, conn, host,
      err, stdout, stderr,
      stdouts, stderrs }=results;

    test(cmd ==='echo command');
    test(conn === 'ssh');
    test(host.match(/^10\.10\.10\.1[1-7]$/) );

    test(err == null);
    test(stdout === 'command');
    test(stderr === '');

    test(stdouts.length == 1);
    test(stdouts[0] == 'command');
    test(stderrs.length == 1);
    test(stderrs[0] == '');
    
  });*/
}, 63 + 27);




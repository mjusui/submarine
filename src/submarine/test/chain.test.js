'use strict';
const { arrows, generate }=require('cons');
const {
  MustPass,
  MustStop,
  CheckFiles,
}=require('submarine/test/Classes/Chain');

const mustpass=new MustPass({
  conn: 'ssh',
  host: '10.10.10.11',
});

const muststop=new MustStop({
  conn: 'ssh',
  host: '10.10.10.11',
})


arrows(
  mustpass,
  muststop,
).arch(
  tests => tests.check().then(
    ret => tests.close().then(
      dummy => ret
    )
  )
).once('Submarine.chain.test.pass', (test, results)=>{
  results.forEach(
    r => {
      const { opt, files, query, stats, exams }=r;
      const { tests, good, bad, total, ok }=exams;

      test(opt.conn === 'ssh');
      test(opt.host === '10.10.10.11');

      test(files.length == 1);
      test(query.pass === 'echo pass');
      test(stats.pass === 'pass');
      test(tests.pass == true);

      test(good == 1);
      test(bad == 0);
      test(total == 1);
      test(ok == true);
    }
  );

      
}, 30).once('Submarine.chain.test.stop', (test, results)=>{
  [
    results[0],
  ].forEach( r => {
    const { opt, files, query, stats, exams }=r;
    const { tests, good, bad, total, ok }=exams;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    test(files.length == 1);
    test(query.pass === 'echo pass');
    test(stats.pass === 'pass');
    test(tests.pass == true);

    test(good == 1);
    test(bad == 0);
    test(total == 1);
    test(ok == true);
  });

  [
    results[1],
    results[2],
  ].forEach( r => {
    const { opt, files, query, stats, exams }=r;
    const { tests, good, bad, total, ok }=exams;

    test(opt.conn === 'ssh');
    test(opt.host === '10.10.10.11');

    test(files.length == 1);
    test(query.pass === 'echo pass');
    test(stats.pass === 'pass');
    test(tests.pass == false);

    test(good == 0);
    test(bad == 1);
    test(total == 1);
    test(ok == false);
  });

}, 30);




'use strict';
require('extended');
const assert=require('assert').strict;
const Conn=require('conn/Class');
const Cont=require('cont/Class');
const { Pub }=require('comm/Classes');
const {
  GetSubJson,
  PutSubJson,
  PostSubJson,
}=require('submarine/consume/json/Classes');
const {
  GetSubHtml,
}=require('submarine/consume/html/Classes');
//const coll=require('coll')
const Coll=require('coll/Class');
const time=require('cache/time/save');
const active=require('cache/active');
const token=require('cache/token');


const Common=class {
  constructor(){
    this.collabs=[];
    this.ctrl=undefined;
  }
  collaborate(...collabs){
    this.collabs.forEach(
      collab => collab.close()
    );

    this.collabs=collabs;

    return this;
  }

  control(opt){
    this.ctrl === undefined
      ? undefined
      : this.ctrl
          .close()

    this.ctrl=active(
      none => this.check(),
      { intv: opt.interval || 3400, }
    );

    return this;
  }

  communicateJson(opt){
    const format=typeof opt.format === 'function'
      ? opt.format
      : r => r;

    const resp=time(
      none => this.check(),
      { intv: opt.cache || 3400, }
    );

    return Pub.port({
      port: opt.port || 30117,
    }).pub(
      new GetSubJson({
        pathname: opt.pathname,
        response: none => (
          this.ctrl === undefined
            ? resp()
            : this.ctrl.cache()
        ).then(format),
      })
    ).pub(
      new PutSubJson({
        pathname: opt.pathname,
        response: time(
          none => this.correct()
        ),
      })
    ).pub(
      new PostSubJson({
        pathname: opt.pathname,
        response: time(
          none => this.call()
        ),
      })
    );
 

  }
  communicateHtml(opt){
    const resp=time(
      none => this.check(),
      { intv: opt.cache || 3400, }
    );

    return Pub.port({
      port: opt.port || 30117,
    }).pub(
      new GetSubHtml({
        pathname: opt.pathname,
        response: none => (
          this.ctrl === undefined
            ? resp()
            : this.ctrl.cache()
        ),
      })
    );
  }

}

const Submarine=class extends Common {
  constructor(opt){
    super();

    this.opt=Object.assign({}, opt);

    this.conn=new Conn(this.opt);

    this.downloads=Array.merge(
      this.fetch()
    ).map(
      opt => new Cont(
        typeof opt.files === 'object'
          ? opt
          : {
              conn: 'sh',
              files: opt,
            }
      )
    );
    this.uploads=this.downloads.map(
      cont => cont.to(this.opt)
    );
 
    this.files=Promise.all(
      this.uploads.map(
        cont => cont.files
      )
    );
  }

  fetch(){ return {}; }
  query(...files){ return {}; }
  format(stats){ return stats; }
  test(stats, ...files){ return {}; }
  command(tests, ...files){ return 'echo Hello Submarine'; }
  batch(tests, ...files){ return 'echo Hello Submarine'; }


  close(){
    return Promise.all(
      Array.merge(
        this.collabs,
        this.ctrl,
        this.downloads,
        this.uploads
      ).map(
        r => r.close()
      )
    );
  }


  current(){
    return Promise.merge(
      ...this.collabs.map(
        collab => collab.check()
      )
    ).then(
      collabs => this.files.then(
        files => ({
          opt: Object
            .assign({}, this.opt),
          collabs: collabs,
          files: files,
          query: this.query({
            collabs: collabs,
            files: files,
          }),
        })
      )
    ).then(
      r => this.conn.query(
        r.query
      ).then(
        stats => Object.assign(r, {
          stats: this.format(stats),
        })
      )
    );
  }

  check(){
    return this.current().then(
      r => {
        const tests=this.test({
          stats: r.stats,
          collabs: r.collabs,
          files: r.files,
        });
        const list=tests.list(v => v);

        const good=list.filter(
          v => v
        ).length;

        const total=list.length;
        const bad=total - good;
        const ok=good == total;

        return Object.assign(r, {
          exams: {
            tests: tests,
            good: good,
            bad: bad,
            total: total,
            ok: ok,
          },
        });
      }
    );
  }

  correct(){
    return this.check().then(
      r => r.exams.ok
        ? r
        : this.conn.run({
          cmd: this.command({
            stats: r.stats,
            collabs: r.collabs,
            files: r.files,
          }),
        }).then(
          exec => Object.assign(r, {
            exec: exec,
          })
        )
    );
  }

  call(){
    return this.check().then(
      r => r.exams.ok
        ? this.conn.run({
          cmd: this.batch({
            stats: r.stats,
            collabs: r.collabs,
            files: r.files,
          }),
        }).then(
          exec => Object.assign(r, {
            exec: exec,
          })
        )
        : r
    );
  }


  static chain(Sub, ...Subs){
    return class extends Chain {
      constructor(opt){
        super(
          none => new Sub(opt),
          0 < Subs.length
            ? new (
                Submarine.chain(
                  ...Subs
                )
              )(opt)
            : undefined
        );
      }
    };
  }

  static collect(hndl){
    return class extends Collect{
      constructor(){
        super(
          none => hndl(
            new Coll()
          )
        );
      }
    }
  }
  /*static hosts(sub, ...hosts){
    return Submarine.collect(sub, {
      type: 'gen',
      coll: 'array',
      array: hosts,
    });
  }
  static collect(sub, ...opts){
    return class extends Collect {
      constructor(){
        super(time(
          () => coll(...opts).then(
            hosts => hosts.map(sub)
          ).catch(assert.collectError),
          1000 * 60 * 12
        ));
      }
    };
  }*/


}




const Collabs=class {
  constructor(...collabs){
    this.cache=Promise.merge(
      ...collabs.map(
        collab => collab.check()
      )
    );
  }
  close(){}

  check(){
    return this.cache;
  }
}


const Chain=class extends Common {
  constructor(sub, next){
    super();

    this.sub=token(sub);
    this.next=
      hndl => next
        ? hndl(next)
        : undefined;
  }


  close(){
    return Promise.merge(
      this.collabs.map(
        collab => collab.close()
      ),
      this.sub().close(),
      this.next(
        next => next.close()
      )
    );
  }
 
  /*collaborate(...collabs){
    const c=new Collabs(
      ...collabs
    );

    this.sub().collaborate(c);
    this.next(
      next => next.collaborate(c)
    );

    return this;
  }*/


  chained(hndl){
    return hndl(this.sub()).then(
      result => Promise.merge(
        result,

        Promise.merge(
          this.sub()
            .check()
        ).then(
          results => results.every(
            r => r.exams.ok
          ) ? this.next(hndl)
            : results
        )
      )
    );
  }


  current(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.chained(
      sub => sub.collaborate(
        collabs
      ).current()
    );
  }
  check(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.chained(
      sub => sub.collaborate(
        collabs
      ).check()
    );
  }
  correct(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.chained(
      sub => sub.collaborate(
        collabs
      ).correct()
    );
  }
  call(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.chained(
      sub => sub.collaborate(
        collabs
      ).call()
    );
  }

}


const Collect=class extends Common {
  constructor(subs){
    super();

    this.subs=(hndl=sub=>sub)=>{
      return subs().then(
        subs => subs.map(hndl)
      ).then(
        subs => Promise.all(subs)
      );
    };

  }

  close(){
    return Promise.merge(
      this.collabs.map(
        collab => collab.close()
      ),
      this.subs(
        sub => sub.close()
      )
    );
  }


  current(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.subs(
      sub => sub.collaborate(
        collabs
      ).current()
    );
  }
  check(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.subs(
      sub => sub.collaborate(
        collabs
      ).check()
    );
  }
  correct(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.subs(
      sub => sub.collaborate(
        collabs
      ).correct()
    );
  }
  call(){
    const collabs=new Collabs(
      ...this.collabs
    );

    return this.subs(
      sub => sub.collaborate(
        collabs
      ).call()
    );
  }

}



module.exports=Submarine;

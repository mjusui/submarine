'use strict';
const { Sub, Resp }=require('comm/Classes');
const ComposeHtml=require('submarine/compose/html');

const OK=new Resp({
  sc: 200,
  headers: {
    'Content-Type': 'text/html',
  },
});

const GetSubHtml=class extends Sub {
  constructor(opt){
    super({
      method: 'GET',
      pathname: opt.pathname,
    });

    this.response=opt.response;
    this.template=new ComposeHtml();
  }

  consume(req, res, done, gone){
    this.response().then(
      opts => OK
        .consumable(res)
        .body(
          this.template.render(opts)
        )
    ).catch(
      errs => OK
        .consumable(res)
        .body(
          this.template.render(errs)
        )
    ).then(done);
  }
}


module.exports={
  GetSubHtml: GetSubHtml,
};

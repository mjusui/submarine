'use strict';
const { Sub }=require('comm/Classes');

const GetSubJson=class extends Sub {
  constructor(opt){
    super({
      method: 'GET',
      pathname: opt.pathname,
    });

    this.response=opt.response;
  }

  consume(req, res, done, gone){
    res.setHeader(
      'Content-Type', 'application/json'
    );
    this.response()
      .then(done)
    .catch(gone);
  }
}

const PutSubJson=class extends Sub {
  constructor(opt){
    super({
      method: 'PUT',
      pathname: opt.pathname,
    });

    this.response=opt.response;
  }

  consume(req, res, done, gone){
    res.setHeader(
      'Content-Type', 'application/json'
    );
    this.response()
      .then(done)
    .catch(gone);
  }
}

const PostSubJson=class extends Sub {
  constructor(opt){
    super({
      method: 'POST',
      pathname: opt.pathname,
    });

    this.response=opt.response;
  }

  consume(req, res, done, gone){
    res.setHeader(
      'Content-Type', 'application/json'
    );
    this.response()
      .then(done)
    .catch(gone);
  }
}

module.exports={
  GetSubJson: GetSubJson,
  PutSubJson: PutSubJson,
  PostSubJson: PostSubJson,
};

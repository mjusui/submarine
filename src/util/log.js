'use strict';

const os=require('os');

const log={};
log.format=(lev='info', ...args)=>{
  console[lev](
    new Date().toISOString(),
    os.hostname(),
    `Submarine[${process.pid}]:`,
    `[${lev.toUpperCase()
        .substring(0,4)}]`,
    ...args);
};

const temp=console.test;

console.test=(...args)=>{
  console.assert(
    args[4] === 'x'
    && args[5] === 'y'
  , 'util/log.format');
};
log.format('test', 'x', 'y');

console.test=temp;

module.exports=log;

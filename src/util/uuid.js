'use strict';
require('extended');

const crypto=require('crypto');

const hex=Array.seq(256,
   i => (i + 0x100)
    .toString(16)
    .substr(1)
);

console.assert(
  hex[0] === '00'
  && hex[1] === '01'
  && hex[10] === '0a'
  && hex[63] === '3f'
  && hex[hex.length-1] === 'ff'
, 'invalid hex table');

const uuid={};

uuid.v4=()=>{
  const rand=Array.from(
    crypto.randomBytes(16)
  );

  rand[6] = (rand[6] & 0x0f) | 0x40;
  rand[8] = (rand[8] & 0x3f) | 0x80;

  const uuid=rand.map( r => hex[r] );

  return [
    ...uuid.slice(0,4),
    '-', ...uuid.slice(4, 6),
    '-', ...uuid.slice(6, 8),
    '-', ...uuid.slice(8, 10),
    '-', ...uuid.slice(10)
  ].join('');
    
};


module.exports=uuid;

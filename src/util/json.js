

const json={};

json.escape=(s)=>{
  return s
    .replace(/\"/g, '\\"')
    .replace(/\\/g, '\\\\')
    .replace(/\//g, '\\/')
    .replace(/\b/g, '\\b')
    .replace(/\f/g, '\\f')
    .replace(/\n/g, '\\n')
    .replace(/\r/g, '\\r')
    .replace(/\t/g, '\\t');
};

json.unescape=(s)=>{
  return s
    .replace(/\\"/g, '\"')
    .replace(/\\\\/g, '\\')
    .replace(/\\\//g, '\/')
    .replace(/\\b/g, '\b')
    .replace(/\\f/g, '\f')
    .replace(/\\n/g, '\n')
    .replace(/\\r/g, '\r')
    .replace(/\\t/g, '\t');
};


module.exports=json;

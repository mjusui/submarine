const crypto=require('crypto');


const base64={};

base64.encode=(data='')=>{
  return Buffer.from(data)
    .toString('base64');
};

base64.decode=(data='')=>{
  return Buffer.from(data, 'base64')
    .toString();
};

console.assert(
  base64.encode('Submarine') === 'U3VibWFyaW5l'
, 'base64.encode');

console.assert(
  base64.decode('U3VibWFyaW5l') === 'Submarine'
, 'base64.decode');

module.exports=base64;

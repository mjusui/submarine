'use strict';

const os = require('os');
const assert=require('assert').strict;

const log=require('util/log');

assert.loadFileError=(...args)=>{
  log.format('error', ...args);
  assert.fail(
    'failed to load files');
};
assert.copyFileError=(...args)=>{
  log.format('error', ...args);
  assert.fail(
    'failed to copy files');
};
assert.collectError=(...args)=>{
  log.format('error', ...args);
  assert.fail(
    'failed to collect hosts');
};



Array.merge=function(...args){
  return args.map(
    arg => Array.isArray(arg)
      ? arg
      : arg === undefined
        ? []
        : [ arg ]
  ).reduce((a , b) => [
    ...a,
    ...b
  ], []);
};


Array.strip=function(arg){
  return Array.isArray(arg)
    && arg.length < 2
      ? arg[0]
      : arg 
}

Array.seq=function(siz, hndl=(val)=>{
  return val;
}){
  const ret=[];
  let cnt=0;
  while(cnt < siz){
    ret.push(
      hndl(cnt)
    );

    cnt++;
  }
  return ret;
};

const subArray=(ary, num, siz, hndl)=>{
  return Array.seq(num, (idx)=>{
    const beg = idx * siz;
    const end = beg + siz;

    return hndl(ary.slice(
      beg, end
    ), idx);
  });
};


Array.prototype.divide=function(num=1, hndl=(ary)=>{
  return ary;
}){
  const siz=Math.ceil(
    this.length/num);

  return subArray(this, num, siz, hndl);
};

Array.prototype.segment=function(siz=1, hndl=(ary)=>{
  return ary;
}){
  const num=Math.ceil(
    this.length/siz);

  return subArray(this, num, siz, hndl);
};

Array.prototype.dict=function(hndl){
  const ret={};

  this.forEach((v, i)=>{
    const { key, val }=hndl(v,i);
    ret[key]=val;
  });

  return ret;
};

Object.prototype.forEach=function(hndl){
  Object.keys(this).forEach((key,idx)=>{
    hndl(this[key],key,idx);
  });
};

Object.prototype.map=function(hndl){
  const ret={};
  this.forEach((v,k)=>{
    const val=hndl(v,k);
    if(val === undefined)
      return;
    ret[k]=val;
  });
  return ret;
};

Object.prototype.list=function(hndl){
  const ret=[];
  this.forEach((v,k)=>{
    const val=hndl(v,k);
    if(val === undefined)
      return;
    ret.push(val);
  });
  return ret;
}

Object.prototype.filter=function(hndl){
  return Object.keys(this).filter((key, idx)=>{
    return hndl(this[key], key, idx);
  }).dict((key, idx)=>{
    return {
      key: key,
      val: this[key],
    };
  });
};

Object.prototype.find=function(hndl){
  return this[Object.keys(this).find((key, idx)=>{
     return hndl(this[key], key, idx);
  })];
};

Object.prototype.some=function(hndl){
  return Object.keys(this).some((key, idx)=>{
    return hndl(this[key], key, idx);
  });
};
Object.prototype.every=function(hndl){
  return Object.keys(this).every((key, idx)=>{
    return hndl(this[key], key, idx);
  });
};

Promise.merge=function(...proms){
  return Promise.all(
    proms
  ).then(
    results => Array.merge(
      ...results
    )
  );
}

Promise.any=function(...pros){
  return new Promise((resl,rejc)=>{
    const d=[];
    const g=[];
    const done=(val)=>{ d.push(val); };
    const gone=(val)=>{ g.push(val); };

    let cnt=0;
    const len=pros.length;
    const none=()=>{
      if(++cnt < len)
        return;

      resl({ done:d, gone:g });
    }

    pros.forEach((pro)=>{
      pro.then(done)
        .catch(gone)
      .finally(none);
    });
  });
};


Promise.State=class {
  constructor(stat){
    this.state=Promise.resolve(stat);
  }

  goes(hndl){
    this.state=this.state.then(hndl);

    return this.state;
  }
}


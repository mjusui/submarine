require('extended');

const { test }=require('cons');

Promise.merge(
  Promise.resolve('a'),
  Promise.resolve([
    'b', 'c',
  ])
).then(test('Promise.merge', (test, results)=>{
  test(results[0] === 'a');
  test(results[1] === 'b');
  test(results[2] === 'c');
  test(results.length == 3)
}, 4));

Promise.any(...[
  new Promise((resl, rejc)=>{
    resl(true);
  }),
  new Promise((resl, rejc)=>{
    rejc(false);
  }),
  new Promise((resl, rejc)=>{
    resl(5);
  })
]).then(test('Promise.any', (test, res)=>{

  test(res.done.length==2);
  test(res.gone.length==1);

  res.done.forEach((val,idx)=>{
    test(val);
  });
  res.gone.forEach((val,idx)=>{
    test(!val);
  });

}, 5));

{
  const State=class extends Promise.State {
    add(b){
      return this.goes(
        a => a + b
      );
    }
  
    mult(b){
      return this.goes(
        a => a * b
      );
    }
  }
  
  const state=new State(3);
  state.add(4);
  state.mult(11).then(test('Promise.State', (test, val)=>{
    test(val == 77);
  }, 1));

}

{
  const a=Array.merge([ 'x', 'y' ], 'z');
  const b=Array.merge('a', undefined, 'b');

  test('Array.merge', (test)=>{
    test(Array.merge().length == 0); 
    test(a[0] === 'x');
    test(a[1] === 'y');
    test(a[2] === 'z');
    test(b[0] === 'a');
    test(b[1] === 'b');
  }, 6)();

  const c=Array.merge('a', null, 'b');

  test('Array.merge.null', test => {
    test(c[0] === 'a');
    test(c[1] === null);
    test(c[2] === 'b');
  }, 3)();
}

{
  const a=Array.strip([ 'z', 'a' ]);

  test('Array.merge', (test)=>{
    test(Array.strip() == undefined); 
    test(Array.strip('x') === 'x'); 
    test(Array.strip([ 'y' ]) === 'y'); 
    test(a[0] === 'z');
    test(a[1] === 'a');
  }, 5)();
}


{
  const res = Array.seq(3, (idx)=>{
    return idx * 2;
  });
  test('Array.seq', (test)=>{
    test(res.length == 3);
    test(res[0] == 0);
    test(res[1] == 2);
    test(res[2] == 4);
  },4)();
}
{
  Array.seq(11).divide(3, test('Array.divide', (test, ary, idx)=>{
    ary.forEach((val, idx2)=>{
      test(val == 4*idx + idx2);
    });
  }, 11));
}
{
  Array.seq(11).segment(4, test('Array.seqment', (test, ary, idx)=>{
    ary.forEach((val, idx2)=>{
      test(val == 4*idx + idx2);
    });
  }, 11));
}
/*{
  [ 'a',
    'b',
    'c',
  ].((v, i)=>{
    return {
      key: v,
      val: `${v}x`,
    };
  }).forEach(test('Array.lift', (test, val, key, idx)=>{
    test(val === `${key}x`);
  }, 3));
 
}*/
{
  ({ a:'x', b:'y' }).forEach(test('Object.forEach', (test, v,k,i)=>{
    test(v==='x' && k==='a' && i==0
      || v==='y' && k==='b' && i==1);
  }, 2));
};


{
  let ret=true;
  const o=({
    a:'b',
    b:'d',
  }).map( (v, k) => k+v )

  test('Object.map', (test)=>{
    test(o.a==='ab');
    test(o.b==='bd');
  }, 2)();

  const o2=({
    a: null,
    b: undefined,
  }).map( v => v );

  test('Object.map.null', test => {
    test(o2.a === null);
    test(o2.b === undefined);
  }, 2)();
};

{
  let ret=true;
  const o=({
    a:'b',
    v: true,
    w: 3,
    x: null,
    b:'d',
    y: undefined,
    z: false,
  }).filter( v => v );

  test('Object.filter', (test)=>{
    test(o.a==='b');
    test(o.v==true);
    test(o.w==3);
    test(o.x==undefined);
    test(o.b==='d');
    test(o.y==undefined);
    test(o.z==undefined);
  }, 7)();
};

{
  const b=({
    a: 'a',
    b: 'b',
    c: 'c',
  }).find( x => x==='b');

  test('Object.find', (test)=>{
     test(b==='b');
  }, 1)();
};

{
  const some=({
    a: 1,
    b: 10,
    c: 99,
  }).some( x => x < 3);

  const some2=({
    a: 5,
    b: 10,
    c: 99,
  }).some( x => x < 3);

  test('Object.some', (test)=>{
    test(some==true);
    test(some2==false);
  }, 2)();

};
{
  const every=({
    a: 1,
    b: 10,
    c: 99,
  }).every( x => x < 100);

  const every2=({
    a: 1,
    b: 100,
    c: 99,
  }).every( x => x < 100);

  test('Object.every', (test)=>{
    test(every==true);
    test(every2==false);
  }, 2)();

};


{
  const list=({
    a:'b',
    b:'d',
  }).list( (v, k) => k+v )

  test('Object.list', (test)=>{
    test(list[0]=='ab');
    test(list[1]=='bd');
  }, 2)();

  const nulls=({
    a: null,
    b: undefined,
    c: null,
  }).list( v => v );

  test('Object.list.null', test => {
    test(nulls[0] === null);
    test(nulls[1] === null);
    test(nulls[2] === undefined);
  }, 3)();

};


let cnt=0

const tarai=(x, y, z)=>{
  cnt++;
  if(x <= y)
    return y;

  return tarai(
    tarai(x-1, y, z),
    tarai(y-1, z, x),
    tarai(z-1, x, y)
  );
};

console.log(
  tarai(21,15,7), cnt
);

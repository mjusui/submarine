

const x=[new Promise((resl, rejc)=>{
  resl('a');
}), 'b', Promise.reject('c'), Promise.resolve('d')];


Promise.all(x.map(
  p => Promise.resolve(p)
)).then(console.log);

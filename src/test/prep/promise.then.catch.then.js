
new Promise((resl, rejc)=>{
  resl('x');
}).then(
  x => console.log(x)
    || 'y'
).catch(
  x => console.log(x)
    || 'z'
).then(console.log);


new Promise((resl, rejc)=>{
  rejc('u');
}).then(
  u => console.log(u)
    || 'v'
).catch(
  u => console.log(u)
    || 'w'
).then(console.log);





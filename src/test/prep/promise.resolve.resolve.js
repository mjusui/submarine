
const a=()=>{
  return new Promise((resl, rejc)=>{
    resl('x');
  }).catch(console.error);
};

const b=()=>{
  a().then(console.log);
};

b();



const first=()=>{
  return new Promise((resl, rejc)=>{
    console.error('first');
    rejc('x');
  });
};


const second=()=>{
  return new Promise((resl, rejc)=>{
    console.log('second');
    resl();
  });
};


second().then(first)
.catch(console.log);

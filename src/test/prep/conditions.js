


console.log(
  (true ? 'a' : 'b') == 'a'
);

console.log(
  (false ? 'a' : 'b') == 'b'
);


console.log(
  'a' === 'a'
  || 'x'==='y'
  ? 'a' : 'b'
);

console.log(
  'a' === 'b'
  || 'x'==='x'
  ? 'a' : 'b'
);

console.log(
  'a' === 'b'
  || 'x'==='y'
  ? 'a' : 'b'
);

console.log(
  'a' === 'a'
  || 'x'==='x'
  ? 'a' : 'b'
);



console.log(
  'a' === 'b'
    ? 'x' : 'b'
);

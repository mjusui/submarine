
const v={
  a: String.raw`
    a \
    b
  `,
};

console.log(
  v.a
);

console.log(
  String.raw`${v.a}`
);

console.log(
  JSON.stringify(v)
);



const t=new Date().getTime();

const pro=new Promise((resl, rejc)=>{
  resl('a');
}).finally(()=>{
  const dt=new Date().getTime() - t;
  console.log(
    dt
  );
});


pro.then(console.log)
.finally((r)=>{
  console.log('x', r)
});



new Promise((resl, rejc)=>{
  resl('x')
}).then();


Promise.all([
 undefined,
 undefined,
]).then(console.log);


Promise.all([ new Promise((resl,rejc)=>{
  resl('a')
}), new Promise((resl, rejc)=>{
  resl('b')
}), undefined]).then(console.log);
  

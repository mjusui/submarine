
new Promise((resl, rejc)=>{
  rejc('a')
}).then((res)=>{
  console.log(`x: ${res}`);
  return res;
}).catch((res)=>{
  console.error(`y: ${res}`);
  return Promise.reject(res);
}).catch(console.error);


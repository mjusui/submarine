


/*new Promise((resl, rejc)=>{
  resl();
}).then((res)=>{
  console.log('a');
}).catch((res)=>{
  console.log('b');
}).finally((res)=>{
  console.log('x');
}).finally((res)=>{
  console.log('y');
});*/

new Promise((resl, rejc)=>{
  resl('x');
}).then(console.log)
.finally(()=>{
  return new Promise((resl, rejc)=>{
    resl('y');
  });
}).then(console.log)
.finally(()=>{
  return new Promise((resl, rejc)=>{
    resl('z');
  });
}).then(console.log)

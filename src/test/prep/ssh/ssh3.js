const exec=require('child_process').exec;


const cmd=String.raw`
  cat /etc/hosts \
  |grep "^192" \
  |awk '{print $2}' \
  |sed -e "s/^target\([1-3]\)$/updated\1/g" \
  > ~/tmp/results.hosts
`;

const ssh=`
  ssh target1 ${cmd.replace(/\n/, '')}
`;

console.log(ssh);
exec(ssh, console.log);
// results.hosts is generated at localhost
// and .replace is annoying



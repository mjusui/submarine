const exec=require('child_process').exec;


const cmd=String.raw`
  cat /etc/hosts \
  |grep "^192" \
  |awk '{print $2}' \
  |sed -e "s/^target\([1-3]\)$/updated\1/g" \
  > ~/tmp/results.hosts
`;

const ssh=`
  cat << 'EOS' | ssh target1 sh
    ${cmd} 
EOS
`;

console.log(ssh);
exec(ssh, console.log);
// The last EOS is unindentable



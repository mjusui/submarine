const exec=require('child_process').exec;


const cmd=String.raw`
  cat /etc/hosts \
  |grep "^192" \
  |awk '{print $2}' \
  |sed -e "s/^target\([1-3]\)$/updated\1/g" \
  > ~/tmp/results.hosts
`;

const enc=Buffer.from(cmd)
  .toString('base64');

const ssh=`
  echo ${enc} | ssh target1 "$(base64 -d)"
`;

console.log(ssh);
exec(ssh, console.log);
// No escaping, No unindentable line.



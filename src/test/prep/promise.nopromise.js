

Promise.all([
  Promise.resolve('x'),
  'y',
  ()=>{
    return Promise.resolve('z')
  },
  ()=>{
    return 'a';
  },
]).then(console.log);



const A = class {
  constructor(){
    console.log(this.a);
    this.a='x';
    console.log(this.a);
  }

  a(){
    return 'a';
  }

  b(){
    return ()=>{
      return this.a();
    };
  }
}


console.log(
  new A().b()()
);


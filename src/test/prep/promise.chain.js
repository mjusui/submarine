

new Promise((resl, rejc)=>{
  console.log('x')
  resl();
}).then(()=>{
  console.log('a')
}).then(()=>{
  console.log('b')
}).catch(()=>{
  console.log('c');
}).then(()=>{
  new Promise((resl, rejc)=>{
    console.log('x')
    resl();
  }).then(()=>{
    console.log('a')
    return new Promise((resl, rejc)=>{
      rejc();
    });
  }).then(()=>{
    console.log('b')
  }).catch(()=>{
    console.log('c');
  });
});

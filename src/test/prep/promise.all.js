
Promise.all([
  new Promise((resl, rejc)=>{
    setTimeout(()=>{
      rejc('w');
    }, 100);
  }),
  new Promise((resl, rejc)=>{
    rejc('x');
  }),
  new Promise((resl, rejc)=>{
    resl('y');
  }),
  new Promise((resl, rejc)=>{
    rejc('z');
  }),
]).then(console.log)
.catch(console.error)
.finally((...args)=>{
  console.log(args.length);
});


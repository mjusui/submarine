

console.log(
  '/tmp', '/tmp/ea3068d4-b228-48a4-bb2f-279c52024e39'
     .match(/^\/tmp\//)
);


console.log(
  'full', '/tmp/ea3068d4-b228-48a4-bb2f-279c52024e39'
     .match(/^\/tmp\/[0-9a-f]{8,8}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{12,12}$/)
);

console.log(
  'not_match', '/tmp/ea3068d4-b2288a4-bb2f-279c52024e39a'
     .match(/^\/tmp\/[0-9a-f]{8,8}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{12,12}$/)
);




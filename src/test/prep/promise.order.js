
const prom=(num=0)=>{
  return new Promise((resl)=>{
    console.log(num);
    resl(num);
  });
};



prom(0).then(()=>{
  return prom(1).then(()=>{
    return prom(2).then(()=>{
      console.log('end0');
    });
  });
});


prom(3).then(()=>{
  return prom(4).then(()=>{
    return prom(5).then(()=>{
      console.log('end1');
    });
  });
});


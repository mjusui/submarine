

const A = class {
  a(){
    return 'a';
  }

  b(){
    return ()=>{
      return this.a();
    };
  }
}


console.log(
  new A().b()()
);


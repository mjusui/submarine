'user strict';


const names={};
const Closer=class {
  constructor(){
    this.closes=[];
  }

  add(...hndls){
    this.closes=[
      ...this.closes,
      ...hndls
    ];

    return this;
  }

  runnable(){
    return (...args)=>{
      const closes=this.closes;
      this.closes=[];

      return Promise.all(
        closes.map(
          close => close(...args)
        )
      );
    };
  }

  static name(opt){
    names[opt.name]=names[opt.name]
      || new Closer();

    return names[opt.name];
  }
}


const closer=new Closer();


module.exports=(hndl, opt={})=>{
  return typeof opt.name === 'string'
    ? Closer.name(opt)
        .add(hndl)
        .runnable()
    : closer.add(hndl)
        .runnable();
};

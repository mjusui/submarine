'use strict';
require('extended');
const uuid=require('util/uuid');
const Conn=require('conn/Class');
const sh=require('conn/sh');


module.exports=class {
  constructor(opt){
    this.conn=new Conn(opt);
  }

  download(opt){
    return sh({
      cmd: `mkdir -p $(dirname ${opt.dst})`,
    }).then(
      dummy => this.conn.download(opt)
    );
  }

  upload(opt){
    return this.conn.run({
      cmd: `mkdir -p $(dirname ${opt.dst})`,
    }).then(
      dummy => this.conn.upload(opt)
    );
  }
}


'use strict';
require('extended');
const uuid=require('util/uuid');
const close=require('cont/close');
const Copy=require('cont/copy/Class');
const Conn=require('conn/Class');



const Cont=class {
  constructor(opt){
    this.opt=opt;
    this.copy=new Copy(opt);
    this.tmpdir=`/tmp/submarine-${uuid.v4()}`;

    this.token=0;
    this.children=[];

  }

  close(){
    return Promise.all([
      new Promise((resl, rejc)=>{
        const poll
          = none => 0 < this.token
            ? setTimeout(poll, 255)
            : resl()
        poll(); 
      }),
      this.files,
      ...this.children,
    ]).then(
      done => this.closer.run({
        cmd: `rm -rf ${this.tmpdir}`,
      })
    );
  }


}

const Upload=class extends Cont {
  constructor(opt){
    super(opt);

    this.files=Promise.resolve(
      opt.files
    ).then( files => Promise.all(
      files.list(
        (src, key) => this.copy.upload({
          src: src,
          dst: `${this.tmpdir}/${src}`,
          
        }).then(dummy => ({
          key: key,
          val: `${this.tmpdir}/${src}`,
        }))
      )
    )).then(
      results => results.dict(
        r => r
      )
    );

    this.closer=this.copy.conn;
  }
}


module.exports=class extends Cont {
  constructor(opt){
    super(opt);

    this.files=Promise.resolve(
      opt.files
    ).then( files => Promise.all(
      files.list(
        (src, key) => this.copy.download({
          src: src,
          dst: `${this.tmpdir}/${src}`,
        }).then(dummy => ({
          key: key,
          val: `${this.tmpdir}/${src}`,
        }))
      )
    )).then(
      results => results.dict(
        r => r
      )
    );

    this.closer=new Conn({
      conn: 'sh',
    });
  }

  reserve(token){
    this.token=token;

    return this;
  }

  to(opt){
    const upload=new Upload(
      Object.assign({}, opt, {
        files: this.files,
      })
    );

    upload.files.then(
      done => 0 < this.token
        && this.token--
    );

    this.children=[
      ...this.children,
      upload.files
    ];


    return upload;
  }

  /*static composite(conts){
    return new Downloads(conts);
  }*/
}

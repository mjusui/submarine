'use strict';
require('extended');
const { arrows }=require('cons');
const Conn=require('conn/Class');
const Cont=require('cont/Class');

const from=new Cont({
  conn: 'ssh',
  host: '10.10.10.11',
  files: {
    a: `${__dirname}/src/a/upload.txt`,
    b: `${__dirname}/src/b/upload.txt`,
  },
});

const to=from.to({
  conn: 'ssh',
  host: '10.10.10.17',
  close: {
    name: 'x',
  },
});

const conn=new Conn({
  conn: 'ssh',
  host: '10.10.10.17',
});

arrows(from, to, to).arch(
  cont => to.files.then(
    files => conn.query(
      files.map(files => `cat ${files}`)
    )
  ).then(
    ret => cont.close()
      .then(dummy => ret)
  ).catch(ret => ret)
).twice('cont.upload', (test, results)=>{
  results.forEach(
    (val, key) => test(val === `cont.upload.${key}`)
  );
}, 4).once('cont.upload.close', (test, result)=>{
  test( !(result.err == undefined) );
}, 1)


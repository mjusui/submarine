'use strict';
require('extended');
const { arrows }=require('cons');
const Conn=require('conn/Class');
const Cont=require('cont/Class');

const cont=new Cont({
  conn: 'ssh',
  host: '10.10.10.11',
  files: {
    a: `${__dirname}/src/a/download.txt`,
    b: `${__dirname}/src/b/download.txt`,
  },
});

const conn=new Conn({
  conn: 'ssh',
  host: '10.10.10.17',
});

arrows(cont, cont).arch(
  cont => cont.files.then(
    files => conn.query(
      files.map( file => `cat ${file}`)
    )
  ).then(
    ret => cont.close()
      .then(dummy => ret)
  ).catch(ret => ret)
).once('cont.download', (test, results)=>{
  results.forEach(
    (val, key) => test(val === `cont.download.${key}`)
  );
}, 2).once('cont.download.close', (test, result)=>{
  test( !(result.err == undefined) );
}, 1);
     


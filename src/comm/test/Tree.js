const Tree=require('Tree');

const tree=new Tree();


tree.leaf('x', 'a', 'b', 'c');
console.assert(tree.branch(
  'a', 'b', 'c'
)==='x', 'Tree.leaf');

console.assert(tree.branch(
  'l', 'm', 'n'
)==undefined, 'Tree.branch.error2');

const mid=tree.branch('a', 'b');
console.assert(
  mid.c==='x',
'Tree.branch.error3');

tree.leaf('y', 'a', 'b', 'c', 'd');
console.assert(
  mid.c.d==='y'
&& tree.branch(
  'a', 'b', 'c', 'd'
)=='y', 'Tree.leaf.error');



const clone=new Tree(tree.root);
console.assert(clone.branch(
  'a', 'b', 'c', 'd'
)==='y', 'Tree.root');

const subtree=new Tree(
  tree.branch('a'));
console.assert(subtree.branch(
  'b', 'c', 'd'
)==='y', 'Tree.branch');

subtree.unleaf('b');
console.assert(subtree.branch(
  'b', 'c'
)==undefined, 'Tree.unleaf');

const { test, generate }=require('cons');
const clust=require('cluster');
const http=require('http');
const url=require('url');
const qs=require('querystring');

const opt={
  protocol: 'http:',
  method: 'GET',
  hostname: 'localhost',
  port: 30117,
  path: '/',
};

if(clust.isMaster){
  const Resp=require('Resp');
  
  const OK=new Resp({ sc: 200, });
  const InternalServerError=new Resp({ sc: 500, });

  const { Pub, Sub }=require('comm/Classes');

  const pub=Pub.port({
    port: 30117,
  });

  const A=class extends Sub {
    accept(req, res, answer){
      answer(url.parse(
        req.url, true
      ).query.q==='a')
    }
    consume(req, res, done, gone){
      OK.consume(res);
      done();
    }
  }
  const a=new A({
    method: 'GET', pathname: '/a'
  });
  a.sub(pub);
  const B=class extends Sub {
    accept(req, res, answer){
      setTimeout(()=>{
        answer(url.parse(
          req.url, true
        ).query.q==='b');
      }, 200);
    }
    consume(req, res, done, gone){
      gone();
    }
  }
  const b=new B({
    method: 'POST', pathname: '/b'
  });
  b.sub(pub);

  const z=new Sub();
  z.sub(pub);

  clust.fork().on('disconnect', ()=>{
    pub.close();
  });
}else{
  const testA=test('Comm.A', (test, resp)=>{
    test(resp.sc == 200);
    test(resp.msg === 'OK');
    test(typeof resp.body === 'object');
  }, 3);
  const testB=test('Comm.B', (test, resp)=>{
    test(resp.sc == 500);
    test(resp.msg === 'Internal Server Error');
    test(typeof resp.body === 'object');
  }, 3);
  const testC=test('Comm.C', (test, resp)=>{
    test(resp.sc == 404);
    test(resp.msg === 'Not Found');
    test(typeof resp.body === 'object');
  }, 3);

  setTimeout(()=>{
    Promise.all(['a','b', ''].map(
      q => new Promise((resl, rejc)=>{
        const req=http.request(Object.assign({}, opt, {
          method: q==='b' ? 'POST' : 'GET',
          path: url.format({
            pathname: `/${q}`,
            query: { q: q, },
          })
        }), (res)=>{
          let data='';
          res.on('data', (chunk)=>{
            data+=chunk;
          });
          res.on('end', ()=>{
            resl({
              sc: res.statusCode,
              msg: res.statusMessage,
              body: JSON.parse(data)
            });
          });
        });
        req.end();
      })
    )).then((resps)=>{;
      resps.forEach(
        generate(
          resp => testA(resp),
          resp => testB(resp),
          resp => testC(resp)
        )
      );

      clust.worker.disconnect();
    });
  }, 100);
}



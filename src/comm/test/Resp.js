const clust=require('cluster');
const http=require('http');
const url=require('url');
const qs=require('querystring');

const opt={
  protocol: 'http:',
  method: 'GET',
  hostname: 'localhost',
  port: 30117,
  path: '/',
};

if(clust.isMaster){
  const Resp=require('Resp');
  
  const OK=new Resp({ sc: 200, });
  const NotFound=new Resp({ sc: 404, });
  const InternalServerError=new Resp({ sc: 500, });

  const serv=http.createServer((req, res)=>{
    const query=url.parse(
      req.url, true
    ).query;

    if(query.q==='a')
      OK.consume(res)
    else if(query.q==='b')
      InternalServerError
        .consumable(res)
        .json(query);
    else 
      NotFound
        .consumable(res)
        .body()
  });
  serv.listen(30117);

  clust.fork().on('disconnect', ()=>{
    serv.close();
  });
}else{
  setTimeout(()=>{
    Promise.all(['a','b'].map(
      q => new Promise((resl, rejc)=>{
        const req=http.request(Object.assign({}, opt, {
          method: q==='b' ? 'POST' : 'GET',
          path: url.format({
            pathname: `/${q}`,
            query: { q: q, },
          })
        }), (res)=>{
          let data='';
          res.on('data', (chunk)=>{
            data+=chunk;
          });
          res.on('end', ()=>{
            resl({
              sc: res.statusCode,
              msg: res.statusMessage,
              body: JSON.parse(data)
            });
          });
        });
        req.end();
      })
    )).then((resps)=>{;
      console.assert(resps.every( resp => (
        resp.sc==200
        && resp.msg==='OK'
        || resp.sc==500
        && resp.msg==='Internal Server Error'
        && resp.body.q==='b'
      )), `Resp: ${JSON.stringify(resps)}`);

      clust.worker.disconnect();
    });
  }, 100);
}



'use strict';

const Tree=class {
  constructor(root={}){
    this.root=root;
  }

  branch(...keys){
    return keys.reduce((root, key)=>{
      return typeof root === 'object'
        ? root[key]
        : root;
    }, this.root);
  }

  leaf(val, ...keys){
    return keys.reduce((root, key, idx)=>{
      root[key] = idx < keys.length-1
        ? typeof root[key] === 'object'
          ? root[key]
          : {}
        : val;
      return root[key];
    }, this.root);
  }

  unleaf(...keys){
    return this.leaf(undefined, ...keys);
  }
}


module.exports=Tree;

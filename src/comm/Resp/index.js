'use strict';

require('extended');

const mesgs={
  100: 'Continue', 101: 'Switching Protocols',
    102: 'Processing', 103: 'Early Hints',
  200: 'OK', 201: 'Created', 203: 'Non-Authoritative Information',
    204: 'No Content', 205: 'Reset Content', 206: 'Partial Content',
    207: 'Multi-Status', 208: 'Already Reported', 226: 'IM Used',
  300: 'Multiple Choices', 301: 'Moved Permanently', 302: 'Found',
    303: 'See Other', 304: 'Not Modified', 305: 'Use Proxy', 306: 'Unused',
    307: 'Temporary Redirect', 308: 'Permanent Redirect',
  400: 'Bad Request', 401: 'Unauthorized', 402: 'Not Found', 403: 'Forbiddent',
    404: 'Not Found', 405: 'Method Not Allowed', 406: 'Not Acceptable',
    407: 'Proxy Authentication Requrired', 408: 'Request Timeout',
    409: 'Conflict', 410: 'Gone', 411: 'Length Required', 412: 'Precondition Failed',
    413: 'Payload Too Large', 414: 'URI Too Long', 415: 'Unsupported Media Type',
    416: 'Range Not Satisfiable', 417: 'Exceptation Failed', 418: 'I\'m a teapot',
    421: 'Misdirected Request', 422: 'Unprocessable Entity', 423: 'Locked',
    424: 'Failed Dependency', 425: 'Too Early', 426: 'Upgrade Required',
    428: 'Precondition Required', 429: 'Too Many Requests', 431: 'Request Header Fields Too Large',
    451: 'Unavailable For Legal Reasones',
  500: 'Internal Server Error', 501: 'Not Implremented', 502: 'Bad Gateway',
    503: 'Service Unabailable', 504: 'Gateway Timeout', 505: 'HTTP Version Not Supported',
    506: 'Variant Also Negotiates', 507: 'Insufficient Storage', 508: 'Loop Detected',
    509: 'Bandwidth Limit Exceeded', 510: 'Not Extended', 511 : 'Network Authentication Required',
};
const mesg=(sc)=>{
  return mesgs[sc];
};

const def={
  sc: 500, msg: undefined,
  json: {}, body: undefined,
};
const Resp=class {
  constructor(opt){
    this.opt=opt || Object.assign({}, def);
  }

  params(opt={}){
    const ret={};
    ret.sc=opt.sc || this.opt.sc;
    ret.msg=opt.msg
       || this.opt.msg
       || mesg(ret.sc);
    ret.headers=Object.assign({},
      this.opt.headers || {},
      opt.headers || {});
    ret.body=opt.body
      || this.opt.body
      || JSON.stringify(opt.json
        || this.opt.json
        || {});
    return ret;
  }

  consume(res, opt={}){
    if(res.finished)
      return;

    const p=this.params(opt);

    res.statusCode=p.sc;
    res.statusMessage=p.msg;
    p.headers.forEach((val, key)=>{
      res.setHeader(key, val);
    });
    res.end(p.body);
  }
  consumable(res, out={}){
    return ['body', 'json'].dict( format => ({
      key: format,
      val: (val)=>{
        const inn={};
        inn[format]=val;

        return this.consume(res, Object.assign(
          {}, out, inn
        ));
      }
    }) );
  }
}



module.exports=Resp;

'use strict';

require('extended');

const http=require('http');
const https=require('https');
const url=require('url');

const Tree=require('Tree');
const Resp=require('Resp');

//const Comm={};

const ports={};
const OK=new Resp({ sc: 200, });
const NotFound=new Resp({ sc: 404, });
const InternalServerError=new Resp({ sc: 500, });
const Pub=class {
  constructor(opt={}){
    this.tree=new Tree();
    
    /*this.proto=http=opt.proto === 'https:'
      ? https : http;*/;
    this.proto=http;
    this.port=opt.port || 80;


    this.server=this.proto.createServer((req, res)=>{
      const sub=this.tree.branch(
        req.method, url.parse(
          req.url, true
        ).pathname);

      new Promise((resl, rejc)=>{
        if(sub){
          sub.accept(req, res, (answer)=>{
            if(answer)
              resl(sub);
            else rejc();
          });
        }else rejc()
      }).then((sub)=>{
        new Promise((resl, rejc)=>{
          sub.consume(
            req, res, resl, rejc
          );
        }).then(OK
          .consumable(res)
          .json)
        .catch(InternalServerError
          .consumable(res)
          .json);
      }).catch(NotFound
        .consumable(res)
        .json);
    });

    this.server.listen(this.port);
  }

  close(){
    this.server.close();
  }

  pub(sub){
    this.tree.leaf(sub,
      sub.method,
      sub.pathname);
    return this;
  }
  unpub(sub){
    this.tree.unleaf(
      sub.method,
      sub.pathname);
    return this;
  }

  static port(opt={}){
    const port=opt.port || 80;

    ports[port] =ports[port] || new Pub({
      protocol: opt.protocol || 'http:',
      port: port,
    });

    return ports[port];
  }
}

const Sub=class {
  constructor(opt={}){
    this.opt=Object.assign({}, {
      method: 'GET',
      pathname: '/',
    }, opt);
    this.method=this.opt.method;
    this.pathname=this.opt.pathname;
  }

  accept(req, res, answer){
    answer(true);
  }
  consume(req, res, done, gone){
    NotFound.consume(res);
    done();
  }

  sub(pub){
    pub.pub(this);
    return this;
  }
  unsub(pub){
    pub.unpub(this);
    return this;
  }
  
}


/*Comm.Pub=Pub;
Comm.Sub=Sub;
Comm.Resp=Resp;
module.exports=Comm;*/
module.exports={
  Pub: Pub,
  Sub: Sub,
  Resp: Resp,
};

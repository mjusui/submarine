const Submarine=require('submarine/Class');

const HelloWorld=class extends Submarine {
  query(){
    return {
      msg: `echo Hello World !`,
    }
  }
  command(){
    return `echo Bye World !`;
  }
}

new HelloWorld({
  conn: 'bash',
}).current()
  .then(console.log);


'use strict';

const http=require('http');
const https=require('https');
require('extended');
const log=require('util/log');

const cons={};

cons.token=require('cache/token');
/*(hndl, times=1)=>{
  let cnt=0;
  let ret=undefined;

  return (...args)=>{
    if(cnt++ < times)
      ret=hndl(...args);    

    return ret;
  };
};*/

const Consume=class {
  constructor(){
    this.queue=[];
    this.code=0

    process.on('beforeExit', ()=>{
      process.exit(
        this.run()
      );
    });
  }

  enqueue(hndl){
    this.queue.unshift(
      cons.token(hndl)
    );
    return this;
  }

  run(){
    return this.queue.filter((hndl)=>{
      const res=hndl();

      if(res.err)
        log.format('error', ...(res.msgs
          ? res.msgs
          : res.msg
            ? [ res.msg ]
            : []
        ));

      return res.err;
    }).length;
  }

  close(hndl){
    const rc = hndl == undefined
      ? this.run()
      : this.enqueue(hndl)
        .run();

    process.exit(rc);
  }
}
const consume=new Consume();


cons.call=(name, hndl, times=1)=>{
  let cnt=0;

  consume.enqueue(()=>{
    return {
      err: !(cnt == times),
      msg: `[${name}]: More or Less calls (${cnt}/${times} calls).`,
    };
  });

  return (...args)=>{
    if(cnt++ < times)
      return hndl(...args);
    else
      consume.close();
  };
};

cons.test=(name, hndl, times=1)=>{
  const test=cons.call(name, (val, msg='')=>{
    if(!val)
      consume.close(()=>{
        return {
          err: true,
          msg: `[${name}]: Test failed. ${msg}`,
        };
      });

    return val;
  }, times);


  return (...args)=>{
    return hndl(test, ...args);
  };
};

cons.error=(name)=>{
  return (...args)=>{
    consume.close(()=>{
      return {
        err: true,
        msgs: [
          `[${name}]: Error has occured.`,
          ...args
        ]
      };
    });
  };
};

cons.time=(name, hndl, time=0, times)=>{
  return cons.call(name, (...args)=>{
    const t=new Date().getTime();
    let dt=0;

    consume.enqueue(()=>{
      const now=new Date().getTime() - t;

      return {
        err: !(0 < dt),
        msg: `[${name}]: Timer not stopped (${now}/${time} ms).`,
      };
    });

    const stop=(msg='')=>{
      dt=new Date().getTime() - t;

      if(0 < time
        && !(dt < time)
      ){
        consume.close(()=>{
          return {
            err: true,
            msg: `[${name}]: Time spent (${dt}/${time} ms). ${msg}`,
          };
        });
      }

      return dt;
    };

    return hndl(stop, ...args);
  }, times);
};

cons.generate=(...hndls)=>{
  let cnt=0;
  const lim=hndls.length;

  return cons.call('cons.generate', (...args)=>{
    return hndls[cnt++](...args);
  }, lim);
};

cons.testers=(name, hndl, lim)=>{
  return [
    cons.test(name, hndl, lim),
    cons.error(`${name}.error`)
  ];
};
const Targets=class {
  constructor(arrows, arch){
    this.cnt=0;
    this.len=arrows.length;
    this.arrows=arrows;
    this.arch=arch;

    consume.enqueue(()=>{
      const { cnt, len, arrows }=this;

      return {
        err: cnt < len,
        msg: `Arrows remain (${cnt}/${len} hit). next=${arrows[cnt]}`,
      };
    });

    this.last=Promise.resolve();
  }
  idx(){
    return this.cnt < this.len
      ? this.cnt++
      : Promise.reject()
    ;
  }
  hit(hndl, hndl2){
    this.last=this.last.then(
      (dummy)=>{
        const idx=this.idx();
        return Promise.resolve(
          this.arch(
            this.arrows[idx], idx
          )
        ).then(hndl)
         .catch(hndl2)
      }
    );

    return this;
  }

  other(...args){
    const testers=cons.testers(...args);

    this.last=this.last.then(
      dummy => Array.seq(
        this.len - this.cnt,
        dummy => Promise.resolve(
          this.arch(
            this.arrows[this.idx()]
          )
        ).then(...testers)
      )
    );

    return this;
  }

  times(times, name, hndl, lim){
    const testers=cons.testers(name, hndl, lim);

    Array.seq(times, idx => this.hit(
      ...testers
    ));

    return this;
  }
  once(...args){
    return this.times(1, ...args);
  }
  twice(...args){
    return this.times(2, ...args);
  }
  thrice(...args){
    return this.times(3, ...args);
  }

}

cons.arrows=(...arrows)=>{
  return {
    arch: arch => new Targets(arrows, arch),
  };
}



module.exports=cons;

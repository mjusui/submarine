const cons=require('cons');

let cnt=0;
console.error=(...args)=>{
  console.assert(
    cnt == 0 && args[4] === '[x]: More or Less calls (0/1 calls).'
    || cnt == 1 && args[4] === 'Arrows remain (2/8 hit). next=b',
  'cons.arrow');

  cnt++;    
}

cons.arrows('a', 'b', 'b', 'c', 'c', 'c', 'd', 'd').arch(
   (x,i) => Promise.resolve(`${x}y${i}`)
).once('a0', (test, val)=>{
  test(val === 'ay0');
}, 1).once('b1', (test, val)=>{
  test(val === 'by1');
}, 1);



cons.arrows('a', 'b', 'b', 'c', 'c', 'c', 'd', 'd').arch(
   x => Promise.resolve(`${x}y`)
).once('a', (test, val)=>{
  test(val === 'ay');
}, 1).twice('b', (test, val)=>{
  test(val === 'by');
}, 2).times(3, 'c', (test, val)=>{
  test(val === 'cy');
}, 3).other('d', (test, val)=>{
  test(val === 'dy');
}, 2).other('x', (test, val)=>{
  test(false);
}, 1);




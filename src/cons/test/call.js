const cons=require('cons');

let cnt=0;
const twice=cons.call('twice', ()=>{
  return ++cnt;
}, 2);

Array.seq(2, (idx)=>{
  const val=twice();

  console.assert(
    0 < val
    && val < 3
  , 'cons.call.return');
});


console.error=(...args)=>{
  console.assert(
    cnt == 2
    && args.length == 5
    && args[4] === '[twice]: More or Less calls (3/2 calls).'
  , 'cons.call.error');
};

twice();

console.assert(
  false,
  'Cans.call.close'
);

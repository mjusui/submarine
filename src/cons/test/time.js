const cons=require('cons');

const time=cons.time('time', (stop, ...args)=>{
  args.forEach(
    arg => setTimeout(()=>{
      console.error=(...args)=>{
        console.assert(
          args.length == 5
          && /^\[time\]: Time spent \([0-9]*\/300 ms\)\. interval=400$/.test(args[4])
        , 'cons.time.spent');
      };

      stop(`interval=${arg}`)
    }, arg)
  );
}, 300, 1);

time(200, 400);



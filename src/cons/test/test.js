const cons=require('cons');

const test=cons.test('test', (test, ...args)=>{
  args.forEach(
    arg => test(arg, `cons.test. val=${arg}`)
  );
}, 3);

test('a', {});

console.error=(...args)=>{
  console.assert(
    args.length == 5
    && args[4] === '[test]: Test failed. cons.test. val=null'
  , 'cons.test.error');
};

test(null);

console.assert(
  false
, 'cons.test.close');

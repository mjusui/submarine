const cons=require('cons');

const time2=cons.time('time2', (stop, ...args)=>{
  setTimeout(()=>{}, 80);
}, 50);

console.error=(...args)=>{
  console.assert(
    args.length == 5
    && /^\[time2\]: Timer not stopped \([0-9]*\/50 ms\)\.$/.test(args[4])
  , 'cons.time2.stop');
};

time2([]);

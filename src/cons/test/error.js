const cons=require('cons');

const error=cons.error('error');

console.error=(...args)=>{
  console.assert(
    args.length == 6
    && args[4] === '[error]: Error has occured.'
    && args[5] === 'This is not intended.'
  , 'cons.error');
};

error('This is not intended.');

console.assert(
  false
, 'cons.error.close');

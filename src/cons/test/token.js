const cons=require('cons');

let cnt=0;
const twice=cons.token(()=>{
  return ++cnt;
}, 2);

Array.seq(10, (idx)=>{
  const val=twice();

  console.assert(
    0 < val
    && val < 3
  , 'cons.token.return');
});

console.assert(
  cnt == 2
, 'cons.token');

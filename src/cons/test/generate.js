const cons=require('cons');

const gen=cons.generate(
  a => 0,
  a => 2,
  a => 3,
);

console.assert(
  gen() == 0,
  'cons.generate'
);
console.assert(
  gen() == 2,
  'cons.generate'
);
console.assert(
  gen() == 3,
  'cons.generate'
);

console.error=(...args)=>{
  console.assert(
    args[4] === '[cons.generate]: More or Less calls (4/3 calls).',
    'cons.generate.call'
  );
};

gen();

'use strict';
require('extended');

const cpus=require('os')
  .cpus()
  .length;

const cons=require('cons');
const coor=require('coor');


let check=(i)=>{
  return i < cpus;
}; 
Array.seq(cpus, cons.test('coor', (test, idx)=>{

  coor((free)=>{
    test(check(idx));

    setTimeout(()=>{
      check=(i)=>{
        return i == cpus;
      };
      free();
    }, 2000);
  });
}, cpus));

let check2=(i)=>{
  return i < 10;
};
Array.seq(10, cons.test('coor.names', (test, idx)=>{

  coor((free)=>{
    test(check2(idx));
      

    setTimeout(()=>{
      check2=(i)=>{
        return i == 10;
      };
      free();
    }, 2000);
  }, {
    name: 'x',
    lim: 10,
    intv: 100,
  });

}, 10));


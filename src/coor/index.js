'use strict';
require('extended');

const cpus=require('os')
  .cpus()
  .length;

const rate=8;
const lim=cpus * rate;

const names={};
const Coor=class {
  constructor(opt){
    this.cnt=0;
    this.lim=opt.lim || lim;
    this.intv=opt.intv || 1; //ms
    this.queue=0;
  }

  lock(){
    this.cnt++;
  }
  free(){
    this.cnt--;
  }
  enqueue(){
    this.queue++;
  }
  dequeue(){
    this.queue--;
  }

  random(){
    return Math.floor(
      Math.random()
      * this.lim
    );
  }
  interval(){
    return this.intv
      * this.queue;
  }

  run(hndl){
    return new Promise((resl, rejc)=>{
      let queueing=true;

      const poll=()=>{
        if(this.cnt < this.lim){
          this.lock();

          if(!queueing){
            this.dequeue();
          }
  
          resl(
            hndl((...args)=>{
              this.free();
  
              return 1 < args.length
                ? args
                : args[0];
            })
          );
          return;
        }
  
        if(queueing){
          queueing=false;
          this.enqueue();
        }

        setTimeout(poll, this.interval());
      };

      setTimeout(poll, this.random());
      //poll();
    });
  }

  static name(opt){
    names[opt.name]=names[opt.name]
      || new Coor(opt);

    return names[opt.name];
  }
}


const coor = new Coor({});


module.exports=(hndl, opt={})=>{
  return typeof opt.name == 'string'
    ? Coor.name(opt)
      .run(hndl)
    : coor.run(hndl);
};



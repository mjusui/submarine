const cons=require('cons');
const save=require('cache/save');

let cnt=0;
const till3=save((val)=>{
  return cnt++ < 3
    ? 'ok:' + val
    : 5 < cnt
        ? 'ok2:' + val
        : Promise.reject('err:' + val);
});


cons.test('cache.save', (test)=>{
  Promise.all([
    till3('a'),
    till3('b'),
    till3('c'),
  ]).then(
    vals => vals.forEach(
      cons.generate(
        v => test(v === 'ok:a'),
        v => test(v === 'ok:b'),
        v => test(v === 'ok:c')
      )
    )
  ).then(
    none => till3('x').then(
      val => test(false)
    ).catch(
      val => {
        test(val.err === 'err:x')
        test(val.saved === 'ok:c')
      }
    )
  ).then(
    none => till3('y').then(
      val => test(false)
    ).catch(
      val => {
        test(val.err === 'err:y');
        test(val.saved === 'ok:c');
      }
    )
  ).then(
    none => till3('z').then(
      val => test('ok2:z')
    )
  );
}, 8)();



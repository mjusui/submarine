const cons=require('cons');
const countcache=require('cache/count/save');

let cnt=0;
const two=countcache((x=0)=>{
  return cnt < 3
    ? cnt++ + x
    : 3 < cnt
        ? cnt++ + x
        : Promise.reject('err:' + (cnt++ + x));
}, {
  lim: 2,
});

cons.test('cache.count.save', (test)=>{
  Promise.all([
    3, 5, 6, 7, 9, 8
  ].map(two)).then(
    vals => vals.forEach(
      cons.generate(
        v => test(v == 3),
        v => test(v == 3),
        v => test(v == 7),
        v => test(v == 7),
        v => test(v == 11),
        v => test(v == 11)
      )
    )
  ).then(
    none => two(12).then(
      val => test(false)
    ).catch(
      val => {
        test(val.err === 'err:15');
        test(val.saved == 11);
      }
    )
  ).then(
    none => two(15).then(
      val => test(false)
    ).catch(
      val => {
        test(val.err === 'err:15');
        test(val.saved == 11);
      }
    )
  ).then(
    none => Promise.all([
      two(22),
      two(25),
      two(21),
    ]).then(
      vals => vals.forEach(
        v => test(v == 26)
      )
    )
  );
}, 13)();



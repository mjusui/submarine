const cons=require('cons');
const countcache=require('cache/count');

let cnt=0;
const five=countcache((x=0)=>{
  return cnt++ + x;
}, {
  lim: 5,
});

cons.test('cache.count', (test)=>{
  test(five(3) == 3);
  test(five(5) == 3);
  test(five(6) == 3);
  test(five() == 3);
  test(five() == 3);
  test(five(7) == 8);
  test(five(2) == 8);
}, 7)();



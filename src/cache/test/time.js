const cons=require('cons');
const timecache=require('cache/time');

let cnt=0;
const five=timecache((x=0)=>{
  return cnt++ + x;
}, {
  intv: 500,
});




cons.test('cache.time', (test)=>{
  test(five(3) == 3);

  setTimeout(()=>{
    test(five(5) == 3);
    test(five(6) == 3);
  }, 100);

  setTimeout(()=>{
    test(five() == 1);
    test(five(10) == 1);
  }, 600);

}, 5)();



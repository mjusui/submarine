'use strict';
const { test, generate }=require('cons');
const active=require('cache/active');


const o=new Date()
  .getTime();
let cnt=0;
const now=active(
  none => cnt++ === 2
    ? Promise.reject(`err${cnt}`)
    : new Date()
       .getTime(),
  { intv: 700, }
);

test('cache.active', test => {
  now.cache().then(
    t => test(
      t - o < 300
    )
  ).catch(console.error);

  setTimeout(
    none => now.cache().then(
      t => test(
        300 < t - o
        && t - o < 800
      )
    ).catch(console.error), 800
  );

  setTimeout(
    none => now.cache().then(
      t => test(false)
    ).catch(
      e => {
        test(e.err === 'err3');
        test(
          300 < e.saved - o
          && e.saved - o < 800
        );
      }
    ), 1500
  );

  setTimeout(
    none => now.cache().then(
      t => test(
        1500 < t - o
        && t - o < 2200
      )
    ).catch(console.error), 2200
  );


  setTimeout(
    none => now.close().then(
      none => test(none === undefined)
    ).catch(console.error),
    2400
  );
}, 6)();


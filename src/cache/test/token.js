const { arrows, generate }=require('cons');
const token=require('cache/token');




const test3=generate(
  (test, val) => test(val === 0),
  (test, val) => test(val === 5),
  (test, val) => test(val === 10)
);


arrows(
  ...Array.seq(5, i => i)
).arch(
  token( x => x*5, { lim: 3 })
).thrice(
  'cache.token.1.2.3',
   test3,
   3
).twice(
  'cache.token.4.5',
  (test, val) => test(val == 10),
  2
);




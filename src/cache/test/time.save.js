const cons=require('cons');
const timecache=require('cache/time/save');

let cnt=0;
const five=timecache((x=0)=>{
  return cnt < 2
    ? cnt++ + x
    : 2 < cnt 
      ? cnt++ + x
      : Promise.reject('err:' + (cnt++ + x));
}, {
  interval: 500
});




cons.test('cache.time.save', (test)=>{
  five(3).then(
    v => test(v == 3)
  );

  setTimeout(()=>{
    [5,6].forEach(
      v => five(v).then(
        v => test(v == 3)
      )
    );
  }, 100);

  setTimeout(()=>{
    [undefined,10].forEach(
      v => five(v).then(
        v => test(v == 1)
      )
    );
  }, 600);

  setTimeout(()=>{
    [22,31].forEach(
      v => five(v).then(
        v => test(false)
      ).catch(
        e => {
          test(e.err === 'err:24');
          test(e.saved == 1);
        }
      )
    );
  }, 1200);

  setTimeout(()=>{
    [19,50].forEach(
      v => five(v).then(
        v => test(v == 22)
      )
    );
  }, 1800);
}, 11)();



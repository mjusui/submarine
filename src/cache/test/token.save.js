const cons=require('cons');
const tokencache=require('cache/token/save');

const three=tokencache((x=0)=>{
  return x == 1
    ? Promise.reject('err:' + (x*5))
    : x*5;
}, {
  limit: 3,
});

cons.test('cache.token.save', (test)=>{

  three(0).then(
    v => test(v == 0)
  ).then(
    none => three(1).then(
      v => test(false)
    ).catch(
      e => {
        test(e.err === 'err:5');
        test(e.saved == 0);
      }
    )
  ).then(
    none => three(2).then(
      v => test(v == 10)
    )
  ).then(
    none => three(100).then(
      v => test(v == 10)
    )
  );
}, 5)();



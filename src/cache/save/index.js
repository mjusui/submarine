
module.exports=(hndl)=>{
  let saved=undefined;

  return (...args)=>{
    return Promise.resolve(
      hndl(...args)
    ).then(
      val => {
        saved=val;

        return val;
      }
    ).catch(
      err => Promise.reject({
        err: err,
        saved: saved,
      })
    );
  };
};


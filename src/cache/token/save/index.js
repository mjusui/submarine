const token=require('cache/token');
const save=require('cache/save');

module.exports=(...args)=>{
  return save(
    token(...args)
  );
};


module.exports=(hndl, opt={})=>{
  const lim=opt.lim
    || opt.limit
    || 1;
  let cnt=0;
  let val=undefined;

  return (...args)=>{
    val = cnt < lim
      ? hndl(...args)
      : val;

    cnt++;

    return val;
  }
};



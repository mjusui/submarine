'use strict';


module.exports=class {
  constructor(hndl, intv=1000){
    this.val=undefined;
    this.err=undefined;
    this.failing=false;

    const refresh=()=>{
      const next=Promise.resolve(
        hndl()
      ).then(
        val => {
          this.val=val;
          this.err=undefined;
          this.failing=false;
        }
      ).catch(
        err => {
          this.err=err;
          this.failing=true;
        }
      ).finally(
        none => {
          this.last=next;

          this.timer=setTimeout(
            refresh,
            intv
          );
        }
      );

      return next;
    };
    this.last=refresh();
  }
  close(){
    return this.last.then(
      none => clearTimeout(
        this.timer
      )
    );
  }

  cache(){
    return this.last.then(
      none => this.failing === false
        ? Promise.resolve(
            this.val
          )
        : Promise.reject({
            err: this.err,
            saved: this.val,
          }) 
    );
  }
}

'use strict';
const Active=require('cache/active/Class');

module.exports=(hndl, opt={})=>{
  return new Active(
    hndl,
    opt.intv
      || opt.interval
  );
};

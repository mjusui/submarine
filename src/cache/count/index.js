
module.exports=count=(hndl, opt={})=>{
  const lim=opt.lim
    || opt.limit
    || 1;
  let cnt=0;
  let val=undefined

  return (...args)=>{
    val = cnt % lim
      ? val
      : hndl(...args);

    cnt++;

    return val;
  };
};


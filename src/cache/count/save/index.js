const count=require('cache/count');
const save=require('cache/save');

module.exports=(...args)=>{
  return save(
    count(...args)
  );
};

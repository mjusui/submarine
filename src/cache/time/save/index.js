const time=require('cache/time');
const save=require('cache/save');

module.exports=(...args)=>{
  return save(
    time(...args)
  );
};

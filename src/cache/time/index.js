
module.exports=(hndl, opt={})=>{
  const intv=opt.intv
    || opt.interval
    || 0;
  let tim=0;
  let val=undefined;

  const refresh=()=>{
    const now=new Date().getTime();
    const ret = now - tim < intv;

    tim = ret
      ? tim
      : now

    return ret;
  };

  return (...args)=>{
    val = refresh()
      ? val
      : hndl(...args); 

    return val;
  };
};



const { test, error }=require('cons');
const Conn=require('conn/Class');

const conn=new Conn({
  conn: 'bash',
});

Promise.all([
  conn.run({ cmd: String.raw` echo -e "x\r\ny" `, }),
  conn.run({ cmd: String.raw` echo -e "x\ry" `, }),
  conn.run({ cmd: String.raw` echo -e "x\ny"`, }),
]).then(
  test('conn.lf', (test, results)=>{
    results.forEach( res => {
      const { stdouts }=res;

      test(stdouts.length == 2);
      test(stdouts[0] === 'x');
      test(stdouts[1] === 'y');
    })
  }, 9)
);





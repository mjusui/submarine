const { test, error }=require('cons');
const Conn=require('conn/Class');


const sh=new Conn({
  conn: 'sh',
});

sh.query({
  ac: String.raw`
    cat ${__dirname}/abc.txt \
    |grep "[a|c]"
  `,

  msg: 'echo query',
}).then(test('conn.query', (test, res)=>{
  const ac=res.ac.split('\n');

  test(Array.isArray(ac));
  test(ac.length == 2);
  test(ac[0] === 'a');
  test(ac[1] === 'c');
  test(res.msg === 'query');
}, 5));




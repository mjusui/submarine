const { test, error }=require('cons');
const sh=require('conn/sync/sh');
const bash=require('conn/sync/bash');
const ssh=require('conn/sync/ssh');



test('conn.sh.sync', (test, res)=>{
  test(res.stdout === '{1..3}');
  test(res.stdouts.length == 1)
  test(res.stdouts[0] === '{1..3}');
}, 3)(sh({
  cmd: 'sleep 1; echo {1..3}',
}));



test('conn.bash.sync', (test, res)=>{
  test(res.stdout === '1 2 3');
  test(res.stdouts.length == 1)
  test(res.stdouts[0] === '1 2 3');
}, 3)(bash({
  cmd: 'sleep 1; echo {1..3}',
}));

test('conn.ssh.sync', (test, res)=>{
  test(res.stdout === '1 2 3');
  test(res.stdouts.length == 1)
  test(res.stdouts[0] === '1 2 3');
}, 3)(ssh({
  host: '10.10.10.11',
  cmd: 'sleep 1; echo {1..3}',
}));


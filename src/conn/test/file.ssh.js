const { test, error }=require('cons');
const Conn=require('conn/Class');

const File=class extends Conn {
  open(){
    return Promise.all([
      this.upload({
        src: `${__dirname}/src/file.txt`,
        dst: `${__dirname}/upload/file.txt`,
      }),
      this.upload({
        src: `${__dirname}/src`,
        dst: `${__dirname}/upload/`,
      }),
      this.download({
        src: `${__dirname}/src/file.txt`,
        dst: `${__dirname}/download/`,
      }),
      this.download({
        src: `${__dirname}/src`,
        dst: `${__dirname}/download/`,
      })
    ]);
  }

  test(){
    return Promise.all([
      this.run({
        cmd: `cat ${__dirname}/upload/file.txt`,
      }),
      this.run({
        cmd: `cat ${__dirname}/upload/src/file.txt`,
      }),
      this.run({
        cmd: `cat ${__dirname}/download/file.txt`,
      }),
      this.run({
        cmd: `cat ${__dirname}/download/src/file.txt`,
      })
    ]);
  }

  close(){
    return this.run({
      cmd: String.raw`
        rm -rf ${__dirname}/upload/file.txt
        rm -rf ${__dirname}/upload/src
        rm -rf ${__dirname}/download/file.txt
        rm -rf ${__dirname}/download/src
        ls ${__dirname}/upload
        ls ${__dirname}/download
      `,
    });
  }
}

const file=new File({
  conn: 'ssh',
  host: '10.10.10.11',
});

file.open().then(test('conn.ssh.file.open', (test, results)=>{

  results.forEach((res)=>{
    test(res.err == null);
    test(res.stdout === '');
    test(res.stderr === '');
    test(res.stdouts.length === 1);
    test(res.stdouts[0] === '');
    test(res.stderrs.length === 1);
    test(res.stderrs[0] === '');
  });

}, 28)).then(
  dummy => file.test().then(
    test('conn.ssh.file.test', (test, results)=>{

      results.forEach((res)=>{
        test(res.err == null);
        test(res.stdout === 'conn.file.test');
        test(res.stderr === '');
        test(res.stdouts.length === 1);
        test(res.stdouts[0] === 'conn.file.test');
        test(res.stderrs.length === 1);
        test(res.stderrs[0] === '');
      });

    }, 28)
  )
).then(
  dummy => file.close().then(
    test('conn.ssh.file.close', (test, res)=>{

      test(res.err == null);
      test(res.stdout === 'empty\nempty');
      test(res.stderr === '');
      test(res.stdouts.length === 2);
      test(res.stdouts[0] === 'empty');
      test(res.stdouts[1] === 'empty');
      test(res.stderrs.length === 1);
      test(res.stderrs[0] === '');
    
    }, 8)
  )
).catch(error('conn.ssh.file.error'));




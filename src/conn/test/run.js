const { test, error }=require('cons');
const Conn=require('conn/Class');


const Test=class extends Conn {
  echo(){
    return this.run({
      cmd: 'echo {1..3}',
    });
  }
}

const sh=new Test({
  conn: 'sh',
});

sh.echo().then(test('conn.sh', (test, res)=>{

  test(res.err == null);
  test(res.stdout === '{1..3}');
  test(res.stderr === '');
  test(res.stdouts.length === 1);
  test(res.stdouts[0] === '{1..3}');
  test(res.stderrs.length === 1);
  test(res.stderrs[0] === '');

}, 7)).catch(error('conn.sh.error'));

const bash=new Test({
  conn: 'bash',
});

bash.echo().then(test('conn.bash', (test, res)=>{

  test(res.err == null);
  test(res.stdout === '1 2 3');
  test(res.stderr === '');
  test(res.stdouts.length === 1);
  test(res.stdouts[0] === '1 2 3');
  test(res.stderrs.length === 1);
  test(res.stderrs[0] === '');

}, 7)).catch(error('conn.bash.error'));



const ssh=new Test({
  conn: 'ssh',
  host: '10.10.10.11',
});

ssh.echo().then(test('conn.ssh', (test, res)=>{

  test(res.err == null);
  test(res.stdout === '1 2 3');
  test(res.stderr === '');
  test(res.stdouts.length === 1);
  test(res.stdouts[0] === '1 2 3');
  test(res.stderrs.length === 1);
  test(res.stderrs[0] === '');

}, 7)).catch(error('conn.ssh.error'));


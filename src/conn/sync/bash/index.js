'use strict';
const sh=require('conn/sync/sh');
const base64=require('util/base64');



module.exports=(opt)=>{
  return sh(Object.assign({}, opt, {
    enc: `echo ${base64.encode(opt.cmd || 'true')} | bash -c "$(base64 -d)"`,
  }));
};

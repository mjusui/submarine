'use strict';
require('extended');
const sh=require('conn/sync/sh');
const base64=require('util/base64');

module.exports=(opt)=>{
  const optstr=Array.merge(
    opt.opt,
    '-o StrictHostKeyChecking=no',
    '-o ConnectTimeout=4'
  ).join(' ');

  return sh(Object.assign({}, opt, {
    enc: `echo ${base64.encode(opt.cmd || 'true')} | ssh ${optstr} ${opt.host} "$(base64 -d)"`,
  }));
};

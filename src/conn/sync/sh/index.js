'use strict';
const exec=require('child_process').execSync;


module.exports=(opt)=>{
  const stdoutstr=exec(
    opt.enc || opt.cmd || 'true'
  ).toString()
  .replace(/\n$/, '');

  return Object.assign({}, opt, {
    stdout: stdoutstr,
    stdouts: stdoutstr.split(/\n/),
  });
};



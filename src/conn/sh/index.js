'use strict';
const { exec }=require('child_process');
const coor=require('coor');


module.exports=(opt)=>{
  return coor( free => new Promise((resl, rejc)=>{
    exec(
      opt.enc || opt.cmd || 'true',
    (err, stdout, stderr)=>{
      const stdoutstr=stdout.replace(/(\r\n|\r|\n)$/, '');
      const stderrstr=stderr.replace(/(\r\n|\r|\n)$/, '');

      const ret=Object.assign({}, opt, {
        err: err,
        stdout: stdoutstr,
        stderr: stderrstr,
        stdouts: stdoutstr.split(/\r\n|\r|\n/),
        stderrs: stderrstr.split(/\r\n|\r|\n/),
      });

      err == null
        ? resl(ret)
        : rejc(ret)

    })
  }).finally(free) );
};



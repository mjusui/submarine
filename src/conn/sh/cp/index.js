'use strict';
const sh=require('conn/sh');

module.exports=(opt)=>{
  const optstr=Array.merge(
    opt.opt,
    '-r'
  ).join(' ');

  return sh(Object.assign({}, opt, {
    cmd: `cp ${optstr} ${opt.src} ${opt.dst}`,
  }));
};

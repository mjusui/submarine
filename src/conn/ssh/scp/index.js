'use strict';
const lim=Math.ceil(
  require('os')
    .cpus()
    .length
  / 2
);
require('extended');
const sh=require('conn/sh');
const coor=require('coor');

module.exports=(opt)=>{
  const optstr=Array.merge(
    opt.opt,
    '-r',
    '-o StrictHostKeyChecking=no',
    '-o ConnectTimeout=4'
  ).join(' ');

  return coor( free => sh(Object.assign({}, opt, {
    cmd: `scp ${optstr} ${opt.src} ${opt.dst}`,
  })).finally(free), {
    name: opt.host,
    lim: lim,
  });
};

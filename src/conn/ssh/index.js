'use strict';
const lim=Math.ceil(
  require('os')
    .cpus()
    .length
  / 2
);
require('extended');
const sh=require('conn/sh');
const base64=require('util/base64');
const coor=require('coor');

module.exports=(opt)=>{
  const optstr=Array.merge(
    opt.opt,
    '-o StrictHostKeyChecking=no',
    '-o ConnectTimeout=4'
  ).join(' ');

  return coor( free => sh(Object.assign({}, opt, {
    enc: `echo ${base64.encode(opt.cmd || 'true')} | ssh ${optstr} ${opt.host} "$(base64 -d)"`,
  })).finally(free), {
    name: opt.host,
    lim: lim,
  });
};

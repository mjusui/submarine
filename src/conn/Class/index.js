'use strict';
require('extended');


module.exports=class {
  constructor(opt){
    this.opt=opt;

    this.conn=require(`conn/${opt.conn}`);
    this.copy=require(`conn/${opt.conn}/cp`);

  }

  run(opt){
    return this.conn(
      Object.assign({}, opt, this.opt)
    );
  }

  query(cmds){
    return Promise.all(
      cmds.list((cmd, key) => this.run({
        key: key,
        cmd: cmd,
      }))
    ).then(
      results => results.dict( r => ({
        key: r.key,
        val: r.stdout /*1 < r.stdouts.length
          ? r.stdouts
          : r.stdout,*/
      }))
    );
  }

  upload(opt){
    return this.copy(Object.assign({}, opt, this.opt, {
      src: opt.src,
      dst: this.opt.host
        ? `${this.opt.host}:${opt.dst}`
        : opt.dst,
    }, this.opt));
  }

  download(opt){
    return this.copy(Object.assign({}, opt, this.opt, {
      src: this.opt.host
        ? `${this.opt.host}:${opt.src}`
        : opt.src,
      dst: opt.dst,
    }, this.opt));
  }


}

# Correct server state

We leaned the method above two section of getting and testing server state. Now we set about changing the target server state.

![correct-server-state-with-ssh.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/03-Correct-server-state/Correct-server-state-with-ssh.gif?inline=false)

```Correct-server-state-with-ssh.js
const Submarine=require('Submarine');

const CorrectServerState=class extends Submarine {
  query(){
    return {
      file_content: String.raw`
        test -r /tmp/submarine/hogehoge \
          && cat /tmp/submarine/hogehoge \
          || echo 'File not readable' \
            >&2
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      file_content_is_hogehoge: stats.file_content === 'hogehoge',
    };
  }

  command(){
    return String.raw`
      mkdir -p /tmp/submarine \
        && echo 'hogehoge' \
          > /tmp/submarine/hogehoge
    `;
  }
 

}


const state=new CorrectServerState({
  conn: 'ssh',
  host: 'localhost',
});
    


state.correct()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => state.close()
);

```


`command` function can be defined returning command as String, executed in the target server to manipulate it. And `state.correct().then` passes an information of command execution, that is like:

```Correct-server-state-with-ssh-1.json
{
  opt: { conn: 'ssh', host: 'localhost' },
  collabs: [],
  files: [ {} ],
  query: {
    file_content: '\n' +
      '        test -r /tmp/submarine/hogehoge \\\n' +
      '          && cat /tmp/submarine/hogehoge \\\n' +
      "          || echo 'File not readable' \\\n" +
      '            >&2\n' +
      '      '
  },
  stats: { file_content: '' },
  exams: {
    tests: { file_content_is_hogehoge: false },
    good: 0,
    bad: 1,
    total: 1,
    ok: false
  },
  exec: {
    cmd: '\n' +
      '      mkdir -p /tmp/submarine \\\n' +
      "        && echo 'hogehoge' \\\n" +
      '          > /tmp/submarine/hogehoge\n' +
      '    ',
    conn: 'ssh',
    host: 'localhost',
    enc: 'echo CiAgICAgIG1rZGlyIC1wIC90bXAvc3VibWFyaW5lIFwKICAgICAgICAmJiBlY2hvICdob2dlaG9nZScgXAogICAgICAgICAgPiAvdG1wL3N1Ym1hcmluZS9ob2dlaG9nZQogICAg | ssh -o StrictHostKeyChecking=no -o ConnectTimeout=4 localhost "$(base64 -d)"',
    err: null,
    stdout: '',
    stderr: '',
    stdouts: [ '' ],
    stderrs: [ '' ]
  }
}
```

`exec` key is newly appearing in the JSON result.

1. cmd: String value returned from `command` function
2. conn: used connector. ex) 'sh', 'bash', and 'ssh' is available.
3. host: the target server when connecter is ssh (in this case, the hostname 'localhost' is set)
4. enc: command actually executed
  * All commands are encoded with base64 inner Submarine and decoded by `base64 -d` command before executed.  
  This makes you avoid escaping problems in ShellScript.  
6. err: `null` when the command exitted zero status code.
7. stdout: output of the command.
8. stdouts: Array of stdout split by `\n`.
9. stderr: error output of the command.
10. stderrs: Array of stderr split by `\n`.


When `correct` function is executed, first `test` funciton is evaluated, and `command` function is only called when one or more test is failed. When all test is passed, `command` is not executed and `correct().then` casts the result of `check().then`. 

```Correct-server-state-with-ssh-2.json
{
  opt: { conn: 'ssh', host: 'localhost' },
  collabs: [],
  files: [ {} ],
  query: {
    file_content: '\n' +
      '        test -r /tmp/submarine/hogehoge \\\n' +
      '          && cat /tmp/submarine/hogehoge \\\n' +
      "          || echo 'File not readable' \\\n" +
      '            >&2\n' +
      '      '
  },
  stats: { file_content: 'hogehoge' },
  exams: {
    tests: { file_content_is_hogehoge: true },
    good: 1,
    bad: 0,
    total: 1,
    ok: true
  }
}
```

In this sample code, run once and 'hogehoge' is written to '/tmp/submarine/hogehoge', then Run again and `test` function has already become returning all true (`/tmp/submarile/hogehoge` is written 'hogehoge' in the former execution), finally `command` call is omitted in any times.

`command` function is to make the target server ideal state. So when the ideal state defined in `test` function is already satisfied, it is not necessary for `command` to be performed.




# Transfer files

Submarine provides an unique feature for file transfering (this is a main update on v1.x from v0.9x). It supports copying file in two directions. One of them is local-to-remote, the other is remote-to-remote via local.

![transfer-files.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/05-Transfer-files/Transfer-files.gif?inline=false)

```Transfer-files.js
const Submarine=require('Submarine');

const TransferFiles=class extends Submarine {
  fetch(){
    return [{
      a: `${__dirname}/files/a.txt`,
    }, {
      conn: 'ssh',
      host: 'localhost',
      files: {
        b: `${__dirname}/files/b.txt`,
        c: `${__dirname}/files/dir`,
      },
   }];
  }

  query(r){
    const { files }=r;
    const files1=files[0];
    const files2=files[1];

    return {
      a: String.raw`
        cat ${files1.a}
      `,

      b: String.raw`
        cat ${files2.b}
      `,

      c: String.raw`
        cat ${files2.c}/c.txt
      `,

      fileA: String.raw`,
        echo ${files1.a}
      `,

      fileB: String.raw`
        echo ${files2.b}
      `,

      fileC: String.raw`
        echo ${files2.c}/c.txt
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      a: stats.a === 'a', // true
      b: stats.b === 'b', // true
      c: stats.c === 'c', // true
    };
  }

  batch(r){
    const { files }=r;
    const files1=files[0];
    const files2=files[1];

    return String.raw`
      cat ${files1.a} ${files2.b} ${files2.c}/c.txt
    `;
  }

}


const transfer=new TransferFiles({
  conn: 'ssh',
  host: 'localhost',
});
    

transfer.call()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => transfer.close()
);
```

Here in `fetch` interface newly appears. This must be implemented returning one object or array of objects. And all these objects must be in two types of formats:

1. key-filepath sets
```format1.json
{
  a: 'fileA',
  b: 'fileB',
}
```
2. connector option with `files` parameter whose value is format 1
```format2.json
{
  conn: 'ssh',
  host: 'serverX',
  files: {
    a: 'fileA',
    b: 'fileB',
  },
}
```

Format 1 is interpretted as local-to-remote transfering (filepaths on localhost is copyied to the target server), and Format 2 is done as remote-to-remote one (filepaths on 'serverX' is transferred to the target server via localhost).

And all files are first transferred to temporary dir(`/tmp/submarine-<uuid>` is hard-coded) on the target server, then you can use these destination paths through the argument of `query`, `test` and `command` functions (it could be got by `r.files`).

So `new TransferFiles` in the sample consequently does transfering localhost(as bash)'s a.txt and localhost(as a remote server)'s b.txt and "dir" directory to the target's `/tmp` dir.


And file-transfering feature should be carefully used because the files you transfer is not removed by itself. `close()` function needs to be called at the end of your code so as to clean up all files used while it runs (this is why `close()` function is done in `finally` clause).

const Submarine=require('Submarine');

const TransferFiles=class extends Submarine {
  fetch(){
    return [{
      a: `${__dirname}/files/a.txt`,
    }, {
      conn: 'ssh',
      host: 'server2',
      files: {
        b: `${__dirname}/files/b.txt`,
        c: `${__dirname}/files/dir`,
      },
   }];
  }

  query(r){
    const { files }=r;
    const files1=files[0];
    const files2=files[1];

    return {
      a: String.raw`
        cat ${files1.a}
      `,

      b: String.raw`
        cat ${files2.b}
      `,

      c: String.raw`
        cat ${files2.c}/c.txt
      `,

      fileA: String.raw`,
        echo ${files1.a}
      `,

      fileB: String.raw`
        echo ${files2.b}
      `,

      fileC: String.raw`
        echo ${files2.c}/c.txt
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      a: stats.a === 'a', // true
      b: stats.b === 'b', // true
      c: stats.c === 'c', // true
    };
  }

  batch(r){
    const { files }=r;
    const files1=files[0];
    const files2=files[1];

    return String.raw`
      cat ${files1.a} ${files2.b} ${files2.c}/c.txt
    `;
  }

}


const transfer=new TransferFiles({
  conn: 'ssh',
  host: 'localhost',
});
    

transfer.call()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => transfer.close()
);


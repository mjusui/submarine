## 5. Shape query result

You might run across the situation where your query is too verbose.

![shape-query-result.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/06-Shape-query-result/Shape-query-result.gif?inline=false)

```Shape-query-result-before.js
const Submarine=require('Submarine');

const CorrectServerState=class extends Submarine {
  query(){
    return {
      file_content: String.raw`
        test -r /tmp/submarine/hogehoge \
          && cat /tmp/submarine/hogehoge \
          || echo 'File not readable' \
            >&2
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      file_content_is_hogehoge: stats.file_content === 'hogehoge',
    };
  }

  command(){
    return String.raw`
      mkdir -p /tmp/submarine \
        && echo 'hogehoge' \
          > /tmp/submarine/hogehoge
    `;
  }
 

}


const state=new CorrectServerState({
  conn: 'ssh',
  host: 'localhost',
});
    


state.correct()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => state.close()
);
```

Same `ip ~` commands are written twice (so it is run twice) and each execution makes `ssh` connection wastfully (usually slower than local exec). This can be replaced bellow.

```Shape-query-result-alter.js
const Submarine=require('Submarine');

const CorrectServerState=class extends Submarine {
  query(){
    return {
      file_content: String.raw`
        test -r /tmp/submarine/hogehoge \
          && cat /tmp/submarine/hogehoge \
          || echo 'File not readable' \
            >&2
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      file_content_is_hogehoge: stats.file_content === 'hogehoge',
    };
  }

  command(){
    return String.raw`
      mkdir -p /tmp/submarine \
        && echo 'hogehoge' \
          > /tmp/submarine/hogehoge
    `;
  }
 

}


const state=new CorrectServerState({
  conn: 'ssh',
  host: 'localhost',
});
    


state.correct()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => state.close()
);
```

`format` interface is helpful to shape the result of query with Node.js features.




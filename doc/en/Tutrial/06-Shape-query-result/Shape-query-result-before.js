const Submarine=require('Submarine');

const ShapeQueryResult=class extends Submarine {
  query(){
    return {
      ip_addrs_171: String.raw`
        ip -o -f inet a \
          |awk '{print $4}' \
        |grep "^171"
      `,

      ip_addrs_172: String.raw`
        ip -o -f inet a \
          |awk '{print $4}' \
        |grep "^172"
      `,

    };
  }
 
}


const shape=new ShapeQueryResult({
  conn: 'ssh',
  host: 'localhost',
});
    


shape.current()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => shape.close()
);

const Submarine=require('Submarine');

const ShapeQueryResult=class extends Submarine {
  query(){
    return {
      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,
    };
  }

  format(stats){
    const ip_addrs = stats.ip_addrs.split('\n');


    return {
      ip_addrs_171: ip_addrs.filter(
        addr => addr.match(/^171/)
      ),

      ip_addrs_172: ip_addrs.filter(
        addr => addr.match(/^172/)
      ),
    };
  }
 
}


const shape=new ShapeQueryResult({
  conn: 'ssh',
  host: 'localhost',
});
    


shape.current()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => shape.close()
);

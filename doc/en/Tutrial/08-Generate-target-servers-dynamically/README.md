** `server1` to `server5` is required to be resolved as `localhost` by editting your `/etc/hosts` or some name servers. **

## 7. Generate target servers dynamically

The servers you control may increase or decrease dynamically. You can generate server names and collect the accessable servers out of these names.

![run-submarine-on-multiple-servers-collected](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/08-Generate-target-servers-dynamically/Run-submarine-on-multiple-servers-collected.gif?inline=false)

```Run-submarine-on-multiple-servers-collected.js
const Submarine=require('Submarine');

const GetServerState=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const Collect=Submarine.collect(
  host => new GetServerState({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'bash',
    cmd: 'echo server{1..5}' },
  { type: 'fil',
    coll: 'func',
    func: hosts => hosts.filter(
      host => host.match(/[2-4]$/)
    ) },
  { type: 'fil',
    coll: 'ping' }

);

const collect=new Collect();


collect.current()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => collect.close()
);
```



The machanism of `Submarine.collect` is almost same as `Submarine.hosts`. Its first argument returns a Submarine instance and other aruguments are expressions to specify a collector which is prepared in Submarine. A collector is interpreted as an array of hostnames. Some collectors can be piped from the former collector. Items of the array last proccessed are in turn instantiated with the first argument(function) of `Submarine.collect`.

And two types of collector exists, `generator` and `filter`. `generator` is for generating an array of hosts while `filter` is for filtering piped one (`type` paremeter ('gen' or 'fil') is required for `generator` or `filter` to be specified).

Collectors built in:  
* generator  
  * array  
    a real array can be specified. piped array is left merged.
    ```
    {
      type: 'gen',
      coll: 'array',
      array: `array you prefer`
    }
    ```
  * bash  
    the stdout of `cmd` executed in bash split by spaces `' '` or line feeds regular expressions `/\r\n|\r|\n/`.  
    a sequence of multi spaces are concidered as a single space.
    ```
    {
      coll: 'bash',
      cmd: `command you run`
    }
    ```
  * sh  
    the stdout of `cmd` executed in sh split by spaces `' '` or line feeds regular expressions `/\r\n|\r|\n/`.  
    a sequence of multi spaces are concidered as a single space.
    ```
    {
      coll: 'sh',
      cmd: `command you run`
    }
    ```
  * ssh  
    the stdout of `cmd` executed in remote host via ssh split by spaces `' '` or line feeds regular expressions `/\r\n|\r|\n/`.  
    a sequence of multi spaces are concidered as a single space.
    ```
    {
      type: 'gen',
      coll: 'ssh',
      host: `ip address or hostname`,
      cmd: `command you run`
    }
    ```
  * submarines  
    only hosts defined in `Submarine.hosts` or `Submarine.collect` and its `test` is all passed  
    detail explanation is bellw in this section
    ```
    {
      type: 'gen',
      coll: 'submarines',
      Class: `Class you defined by Submarine.hosts or Submarine.collect`
    }
    ```
* filter  
  * func  
    filter piped array by function in node.js
    ```
    {
      type: 'fil',
      coll: 'func',
      func: `function`
    }
    ```
  * ping  
    filter piped array by pinging.  
    host pinged successfully is left in array.  
    ```
    {
      type: 'fil',
      coll: 'ping',
      opt: `Array of ping options. concatenated with space`
    }
    ```
  * ssh  
    filter piped array by sshing.  
    host executed command via ssh successfully(return code is 0) is left in array.  
    ```
    {
      type: 'fil',
      coll: 'ssh',
      cmd: `command you run`,
      opt: `Array of ssh options. concatenated with space`
    }
    ```




You would like to generate hosts from another Class of `Submarine.hosts` or `Submarine.collect`, then `submarines` generator is available.


```Run-submarine-on-multiple-servers-collected-by-submarine.js
const Submarine=require('Submarine');


const GetServerState=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const MidServer=class extends Submarine {
  query(){
    return {
      server_name: `echo ${this.opt.host}`,
    };
  }
  test(r){
    const { stats }=r;

    return {
      server_in_the_middle:
         stats.server_name.match(/[2-4]$/),
    };
  }
}
const MidServers=Submarine.collect(
  host => new MidServer({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'bash',
    cmd: 'echo server{1..5}', },
);

const Collect=Submarine.collect(
  host => new GetServerState({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'submarines',
    Class: MidServers, },
);

const collect=new Collect();


collect.current()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => collect.close()
);
```

In this code, the hosts, passed to `MidServer` class instantiation and its `test` function returns all true, is handed in to `GetServerState` class instantiation.


`Submarine.hosts` and `Submarine.collect` function is, like this, able to pass hosts to the next `Submarine.hosts` or `Submarine.collect` functions.



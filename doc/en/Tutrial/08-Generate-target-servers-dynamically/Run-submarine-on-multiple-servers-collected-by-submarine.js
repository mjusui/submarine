const Submarine=require('Submarine');


const GetServerState=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const MidServer=class extends Submarine {
  query(){
    return {
      server_name: `echo ${this.opt.host}`,
    };
  }
  test(r){
    const { stats }=r;


    return {
      server_in_the_middle:
         stats.server_name.match(/[2-4]$/),
    };
  }
}
const MidServers=Submarine.collect(
  host => new MidServer({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'bash',
    cmd: 'echo server{1..5}', },
);

const Collect=Submarine.collect(
  host => new GetServerState({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'submarines',
    Class: MidServers, },
);

const collect=new Collect();


collect.current()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => collect.close()
);

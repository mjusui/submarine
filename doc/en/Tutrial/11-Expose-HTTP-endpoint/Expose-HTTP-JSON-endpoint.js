const Submarine=require('Submarine');

const ExposeHttpEndpoint=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),

      tail: stats.tail
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      tail: String.raw`
        sudo tail -n 6 \
          /var/log/syslog
      `,
    };
  }
}


const endpoint=new ExposeHttpEndpoint({
  conn: 'ssh',
  host: 'localhost',
});


endpoint.communicateJson({
  port: 30117,
  pathname: '/logs/syslog',
  cache: 5 * 1024,
  format: r => r.stats,
});

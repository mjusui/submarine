# Expose HTTP endpoint

An interface (query, test, command) defined in your class extends `Submarine` can be exposed as HTTP endpoint returning JSON with `communicateJson function.

![expose-http-json-endpoint.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/11-Expose-HTTP-endpoint/Expose-HTTP-JSON-endpoint.gif?inline=false)
![expose-http-html-endpoint.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/11-Expose-HTTP-endpoint/Expose-HTTP-HTML-endpoint.gif?inline=false)

```Expose-HTTP-JSON-endpoint.js
const Submarine=require('Submarine');

const ExposeHttpEndpoint=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),

      tail: stats.tail
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      tail: String.raw`
        sudo tail -n 6 \
          /var/log/syslog
      `,
    };
  }
}


const endpoint=new ExposeHttpEndpoint({
  conn: 'ssh',
  host: 'localhost',
});


endpoint.communicateJson({
  port: 30117,
  pathname: '/logs/syslog',
  cache: 5 * 1024,
  format: r => r.stats,
});
```

You run this code and the shell command,

```
$ curl lcoalhost:30117/logs/syslog
```

then you will get a JSON-formatted result of `current` function.

`communicateJson` function is implemented in `Submarine`, which takes an option(detailed bellow) and then starts an HTTP JSON server to export interface you defined.

* An option taken by `communicateJson` function:  
  * pathname: URL pathname you want to service (required)
  * port: URL port you want to service (default 30117)
  * cache: cache time(milli second) you want to cache response (default 0, affects responses to `GET` request)
  * format: function that your JSON response is formatted by

All queries and body of requests are ignored, that is why security risk of injection could be avoided.

And PUT or POST request can be done, then the former is to run `correct` function, the latter is to run `call` function.


In addition, you can expose your `query` and `test` results as HTML expressions with `communicateHtml` function.

```Expose-HTTP-HTML-endpoint.js

~ same as above ~

endpoint.communicateHtml({
  port: 30117,
  pathname: '/logs/syslog.html',
  cache: 5 * 1024,
});
```

You can type your browser http://<your serving ip address>:30117/logs/syslog.html, then Enter and you'll get the page.

In HTML endpoint, you cannot specify format option, and PUT and POST requests are ignored.

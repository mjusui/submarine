# Get server state

You can get hostname, ip addresses, cpu count and mem total from your server currently logged in. Save this code to a file and Run `node /path/to/file`.

![get-server-state-with-bash.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/01-Get-server-state/Get-server-state-with-bash.gif?inline=false)

```Get-server-state-with-bash.js
const Submarine=require('Submarine');

const GetServerState=class extends Submarine {
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }


}


const state=new GetServerState({
  conn: 'bash',
});


state.current()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => state.close()
);
```

Let us walk through line by line.  
1. `require('Submarine')` imports Submarine Class.
2. `class extends Submarine` defines new Class handed over the features predefined in Submarine Class.
3. `query` is a original thing in GetServerState class and returns a key-value sets. keys are just identifiers. values are commands executed in the target server. In this section, four queries(command) are defined (`hostname -s` and `ip -o -f ...` etc.). Their results(stdout) are the server state you get.
4. `String.raw` means skipping all escapes and therefore leaving the string value as it is. This syntax is a JavaScript feature, preventing some characters, such as `\`, `$`, `'`, `"` etc, from being prefixed by escape character `\`. We recommend using this feature if your command seems too long or includes `line feed` or other characters likely to be escaped.
5. `new GetServerState({ conn: 'bash' })` instantiates your GetServerState Class. `conn` is abbreviation of Connector which means your `query` runs in bash(localhost). `conn` might be `sh`, `bash` and `ssh`. And a `host` option(IP address or hostname) is required when `ssh` is set.
6. `state.current().then` is a real point your queries are executed. `current` means the current state of the target server. And the funciton you passed to `then` is called when all your queries are complete, which is asynchronous.
7. `console.log` outputs the `stats`(key-value sets). keys are the same as defined in `query`. values are the results(stdouts) of the command you defined in `query`. The result could be an Array(ex. [a, b, c]) when the stdout became multi-line.
8. `catch` is called when one or more queries you define is failed and error object is passed to `console.error`, then the reason of the errors have occurred is printed.
9. `finally` is called whenever your queries are successed or failed, and `close()` is a completion process of your `state` instance (the explanation appears in the following Tutrials).

You can also query to a remote host via ssh (connecting to 127.0.0.1(localhost) instead in this example)

```Get-server-state-with-ssh.js
const state=new GetServerState({
  conn: 'ssh',
  host: 'server1',
  opt: [
    '-l mjusui',
    '-i /home/mjusui/.ssh/id_rsa',
    '-o ConnectTimeout=10',
  ],
});
```

1. `ssh` is set as connector, so `host` is too set.
2. `opt` is an optional parameter, handed to ssh command with its items concatenated by space `' '` if it is provided as an Array. The case that a String in one line is set to could also occur, in which the String value is evaluated intact as ssh option. And `opt` is at any time followed by two default options `-o StrictHosKeyChecking=no` and `-o ConnectTimeout=4`. These options are ,in ssh syntax, allowed to be specified multiple times and the former is effective. you can consequently override these options only if you explicitly specify them.


# JSON response

If you are successfully done your code, you will get the JSON-formatted value like this.

```Get-server-state-with-ssh.json
{
  opt: {
    conn: 'ssh',
    host: 'localhost',
    opt: [
      '-l mjusui',
      '-i /home/mjusui/.ssh/id_rsa',
      '-o ConnectTimeout=10'
    ]
  },
  collabs: [],
  files: [ {} ],
  query: {
    hostname: 'hostname -s',
    ip_addrs: "\n        ip -o -f inet a \\\n          |awk '{print $4}'\n      ",
    cpu_count: '\n' +
      '        cat /proc/cpuinfo \\\n' +
      "          |grep '^processor\\s*: ' \\\n" +
      '          |wc -l\n' +
      '      ',
    mem_kB: '\n' +
      '         cat /proc/meminfo \\\n' +
      "           |grep '^MemTotal: ' \\\n" +
      "           |awk '{print $2}'\n" +
      '      '
  },
  stats: {
    hostname: 'ubu1804-masq-embryo',
    ip_addrs: '127.0.0.1/8\n' +
      '172.17.0.3/32\n' +
      '10.10.10.1/32\n' +
      ... ,
    cpu_count: '4',
    mem_kB: '8172640'
  }
}
```

1. `opt` is what you specify when the instance of `GetServerState` is created with `new`.
2. `collabs` is the thing for collaborating with other servers (details are in the 9. [Collaborate with other servers](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/09-Collaborate-with-other-servers))
3. `files` is the thing to transfer files (details are in the 5. [Transfer files](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/05-Transfer-files))
4. `query` is the ShellScripts you wrote as the returned value of `query` function.
5. `stats` is the results (stdouts) of your query.




const Submarine=require('Submarine');

const GetServerState=class extends Submarine {
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const state=new GetServerState({
  conn: 'bash',
});


state.current()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => state.close()
);

# Test server state

You can make sure your server intended to be configured or operating.

![test-server-state-with-ssh.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/02-Test-server-state/Test-server-state-with-ssh.gif?inline=false)

```Test-server-state-with-ssh.js
const Submarine=require('Submarine');

const TestServerState=class extends Submarine {
  query(){
    return {
      which_none: String.raw`
        which none \
          2> /dev/null \
        || exit 0
      `,

      which_node: String.raw`
        which node \
          2> /dev/null \
        || exit 0
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      none_is_not_executable: stats.which_none === '',
      node_is_executable: stats.which_node,
    };
  }


}


const tests=new TestServerState({
  conn: 'ssh',
  host: 'localhost',
});



tests.check()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => tests.close()
);
```

Newly introduced should be the `test` interface, actually evaluated at the point of `check().then`. `r`, passed as the argument of `test` function, is the Object returned by `current` function, which you see in the previous section. And `stats` is the results of two queries defined in `query` function, each of which are to locate `none` or `node` command path and force exitting successfully even if its executable is not found. And the values of `stats` are verified in `test` function. One verification result is tagged by key of `none_is_not_executable`, Another one is done by `node_is_executable`. They seems to be truthy. `tests.check().then` eventually results in sets having these contents:

```Test-server-state-with-ssh.json
{
  opt: { conn: 'ssh', host: 'localhost' },
  collabs: [],
  files: [ {} ],
  query: {
    which_none: '\n        which none \\\n          2> /dev/null \\\n        || exit 0\n      ',
    which_node: '\n        which node \\\n          2> /dev/null \\\n        || exit 0\n      '
  },
  stats: { which_none: '', which_node: '/usr/bin/node' },
  exams: {
    tests: {
      none_is_not_executable: true,
      node_is_executable: '/usr/bin/node'
    },
    good: 2,
    bad: 0,
    total: 2,
    ok: true
  }
}
```

New object, its key is `exams`, is added to the result of JSON value. 

1. `tests`: an Object returned by `test` function
2. `good`: count of truthy values ot `tests`
3. `bad`: count of falsy values ot `tests`
4. `total`: count of all values ot `tests`
5. `ok`: `true` if all values of `tests` are truthy, or else `false` in the other case.





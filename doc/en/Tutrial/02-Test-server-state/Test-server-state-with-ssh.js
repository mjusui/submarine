const Submarine=require('Submarine');

const TestServerState=class extends Submarine {
  query(){
    return {
      which_none: String.raw`
        which none \
          2> /dev/null \
        || exit 0
      `,

      which_node: String.raw`
        which node \
          2> /dev/null \
        || exit 0
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      none_is_not_executable: stats.which_none === '',
      node_is_executable: stats.which_node,
    };
  }
 

}


const tests=new TestServerState({
  conn: 'ssh',
  host: 'localhost',
});
    


tests.check()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => tests.close()
);

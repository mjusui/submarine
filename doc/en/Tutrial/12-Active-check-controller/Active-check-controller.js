const Submarine=require('Submarine');

const ExposeHttpEndpoint=class extends Submarine {
  query(){
    return {
      date: 'sleep 2; date',
    };
  }
}


const endpoint=new ExposeHttpEndpoint({
  conn: 'ssh',
  host: 'localhost',
}).control({
  interval: 3000,
});


endpoint.communicateHtml({
  port: 30117,
  pathname: '/date.html',
  cache: 12000,
});

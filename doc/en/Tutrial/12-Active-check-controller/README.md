# Active check controller

You may want that a test of your server is regularly done in background and the result is cached and always rendered immediately to your browser.

In such case, you can setup a controller by which `check` function is constantly done and cacled by interval you specify.

```Active-check-controler.js
const Submarine=require('Submarine');

const ExposeHttpEndpoint=class extends Submarine {
  query(){
    return {
      date: 'sleep 2; date',
    };
  }
}


const endpoint=new ExposeHttpEndpoint({
  conn: 'ssh',
  host: 'localhost',
}).control({
  interval: 3000,
});


endpoint.communicateHtml({
  port: 30117,
  pathname: '/date.html',
  cache: 12000,
});
```

`control` function deploys a controller with interval option and `cache` option of `communicateHtml` and `communicateJson` function is ignored when controller exists.

So the code above does simple shell command of `date` by 3 second(2 second `sleep` comes before the `date` is done), and you can see the result of `date` command being updated by about 5 second in your browser.




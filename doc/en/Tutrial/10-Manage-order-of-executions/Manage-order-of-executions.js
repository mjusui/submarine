const Submarine=require('Submarine');

const Step1=class extends Submarine {
  filepath(){
    return '/tmp/submarine/step1.txt';
  }
  query(){
    const filepath=this.filepath();

    return {
      step: String.raw`
        test -r \
          ${filepath} \
        && cat ${filepath} \
        || echo ''
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      finished: !(stats.step === ''),
    };
  }

  command(){
    const filepath=this.filepath();

    return String.raw`
      mkdir -p \
        $(dirname ${filepath}) \
      && touch \
        ${filepath} \
      && uuidgen -r \
        > ${filepath}
    `;
  }
}


const Step2=class extends Step1 {
  filepath(){
    return '/tmp/submarine/step2.txt';
  }

  command(){
    const filepath=this.filepath();

    return String.raw`
      cat \
        /tmp/submarine/step1.txt \
      > ${filepath}
    `;
  }
}


const Steps=Submarine.chain(
  Step1,
  Step2
);

const steps=new Steps({
  conn: 'ssh',
  host: 'localhost',
});


steps.correct()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => steps.close()
);


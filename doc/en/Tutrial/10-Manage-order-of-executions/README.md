## Manage order of executions

`chain` function is prepared for management of execution order (`composite` function in v0.9x is migrated to `chain` function in v1.x).

![manage-order-of-executions.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/10-Manage-order-of-executions/Manage-order-of-executions.gif?inline=false)

```Manage-order-of-executions.js
const Submarine=require('Submarine');

const Step1=class extends Submarine {
  filepath(){
    return '/tmp/submarine/step1.txt';
  }
  query(){
    const filepath=this.filepath();

    return {
      step: String.raw`
        test -r \
          ${filepath} \
        && cat ${filepath} \
        || echo ''
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      finished: !(stats.step === ''),
    };
  }

  command(){
    const filepath=this.filepath();

    return String.raw`
      mkdir -p \
        $(dirname ${filepath}) \
      && touch \
        ${filepath} \
      && uuidgen -r \
        > ${filepath}
    `;
  }
}


const Step2=class extends Step1 {
  filepath(){
    return '/tmp/submarine/step2.txt';
  }

  command(){
    const filepath=this.filepath();

    return String.raw`
      cat \
        /tmp/submarine/step1.txt \
      > ${filepath}
    `;
  }
}


const Steps=Submarine.chain(
  Step1,
  Step2
);

const steps=new Steps({
  conn: 'ssh',
  host: 'localhost',
});


steps.correct()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => steps.close()
);
```

In this code, two steps(`Step1` and `Step2`) are in turn executed. `chain` function literally chains `Submarine` classes. This means that a class is not evaluated if the privious `check` is not passed. In `Step2`, '/tmp/submarine/step1.txt' is used, which is dependent with the process of `Step1` but `Step2` class doesn't have to inspect again the same tests of `Step1`, that is because `chain` function controls that `Step2` class is instantiated and runs command after all tests of `Step1` is passed.



# Run if test is passed

In `correct` function, command defined in `command` function is run only when test is failed, while you can run your command in the case that test is passed. You define a command string in `batch` function, then `call` function does it.
`

![run-if-test-is-passed.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/04-Run-if-test-is-passed/Run-if-test-is-passed.gif?inline=false)

```Run-if-test-is passed.js
const Submarine=require('Submarine');

const RunRecursively=class extends Submarine {
  query(){
    return {
      processes: String.raw`
        ps aux \
          |grep -v grep \
          |grep node \
        |wc -l
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      process_running: stats.processes*1 < 4, 
    };
  }

  batch(){
    return String.raw`
      node ${__dirname}/Run-if-test-is-passed.js
      sleep 5
    `;
  }
 

}


const run=new RunRecursively({
  conn: 'sh',
});
    


run.call()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => run.close()
);
```



`RunRecursively` class is defined as it counts node processes and if they are less than 4, run new node process. So this code runs node processes three times recursively (including itself).




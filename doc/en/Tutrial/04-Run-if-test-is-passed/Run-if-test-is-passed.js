const Submarine=require('Submarine');

const RunRecursively=class extends Submarine {
  query(){
    return {
      processes: String.raw`
        ps aux \
          |grep -v grep \
          |grep node \
        |wc -l
      `,
    };
  }

  test(r){
    const { stats }=r;

    return {
      process_running: stats.processes*1 < 4, 
    };
  }

  batch(){
    return String.raw`
      node ${__dirname}/Run-if-test-is-passed.js
      sleep 5
    `;
  }
 

}


const run=new RunRecursively({
  conn: 'sh',
});
    


run.call()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => run.close()
);

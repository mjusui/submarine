# Run Submarine on Multiple servers

Your class extended from `Submarine` can be applied to multiple servers by `Submarine.hosts` function.

![run-submarine-on-multiple-servers.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/07-Run-submarine-on-multiple-servers/Run-submarine-on-multiple-servers.gif?inline=false)

```Run-submarine-on-multiple-servers.js
const Submarine=require('Submarine');

const GetServerState=class extends Submarine {
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const Hosts=Submarine.hosts(
  host => new GetServerState({
    conn: 'ssh',
    host: host,
  }),

  'localhost',
  'localhost',
  'localhost'
);

const hosts=new Hosts();

hosts.current()
  .then(console.log)
  .catch(console.error)
.finally(
  _ => hosts.close()
);
```


The first argument of `Submarine.hosts` is a function which the arguments after the second one is passed to one by one and an instance of your class is returned from. The arguments from second to last are IP addresses or hostnames. And `Submarine.hosts` itself returns a Submarine-like class having four functions of `current`, `check`, `correct` and `call`. Each of these functions returns an array of the values resulting from the same-named functions of all instances generated inner `Submarine.hosts`.



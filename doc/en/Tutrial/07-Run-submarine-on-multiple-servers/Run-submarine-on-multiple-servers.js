const Submarine=require('Submarine');

const GetServerState=class extends Submarine {
  format(stats){
    return Object.assign(stats, {
      ip_addrs: stats.ip_addrs
        .split(/\r\n|\r|\n/),
    });
  }
  query(){
    return {
      hostname: 'hostname -s',

      ip_addrs: String.raw`
        ip -o -f inet a \
          |awk '{print $4}'
      `,

      cpu_count: String.raw`
        cat /proc/cpuinfo \
          |grep '^processor\s*: ' \
          |wc -l
      `,

      mem_kB: String.raw`
         cat /proc/meminfo \
           |grep '^MemTotal: ' \
           |awk '{print $2}'
      `
    };
  }
 

}


const Hosts=Submarine.hosts(
  host => new GetServerState({
    conn: 'ssh',
    host: host,
  }),

  'localhost',
  'localhost',
  'localhost'
);

const hosts=new Hosts();

hosts.current()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => hosts.close()
);

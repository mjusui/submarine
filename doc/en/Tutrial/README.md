
# Prerequisites

Some programing skill for JavaScript(Node.js) may be needed for you to learn Submarine.js. It might be more required you are familiar with Shell sctipt.

And you need to be able to ssh your 'localhost' with no password.


# Setup

https://gitlab.com/mjusui/submarine/tree/latest


# Tutrials

1. [Get server state](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/01-Get-server-state)
2. [Test server state](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/02-Test-server-state)
3. [Correct server state](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/03-Correct-server-state)
4. [Run if test is passed](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/04-Run-if-test-is-passed)
5. [Transfer files](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/05-Transfer-files)
6. [Shape query result](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/06-Shape-query-result)
7. [Run submarine on multiple servers](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/07-Run-submarine-on-multiple-servers)
8. [Generate target servers dynamically](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/08-Generate-target-servers-dynamically)
9. [Complex test - relationship of servers](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/09-Complex-test-relationship-of-servers)
10. [Manage order of executions](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/10-Manage-order-of-executions)
11. [Expose HTTP endpoint](https://gitlab.com/mjusui/submarine/tree/master/doc/en/Tutrial/11-Expose-HTTP-endpoint)


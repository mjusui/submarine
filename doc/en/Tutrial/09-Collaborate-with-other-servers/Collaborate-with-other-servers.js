const Submarine=require('Submarine');


const GetVolSize=class extends Submarine {
  query(){
    return {
      volkb_sizes: String.raw`
        df -P \
          |grep -v "^Filesystem" \
          |awk '{print $3+$4}'
      `,
    };
  }
  format(stats){
    return {
      volkb_largest: stats.volkb_sizes
        .split(/\r\n|\r|\n/)
        .sort(
          (a, b) => b*1 - a*1
        )[0], // largest volume is chosen.
    };
  }

  test(r){
    const { stats }=r;

    return {
      size_enough:
        2 * 1024 * 1024 < stats.volkb_largest,
    };
  }
}

const TestTotalVolSize=class extends Submarine {
  test(r){
    const { collabs }=r;

    return {
      each_size_enough:
        collabs.every(
          collab => collab.exams.ok
        ),
      total_size_enough:
        4 * 1024 *1024 < collabs.reduce((siz, collab)=>{
          return siz + collab.stats.volkb_largest;
        }, 0), // larger than 4GB
    };
  }
}


const GetVolSizes=Submarine.collect(
  host => new GetVolSize({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'bash',
    cmd: 'echo server{1..5}', },
  { type: 'fil',
    coll: 'func',
    func: hosts => hosts.filter(
      host => host.match(/[2-4]$/)
    ), },
  { type: 'fil',
    coll: 'ping', }
);


const enough=new TestTotalVolSize({
  conn: 'sh',
}).collaborate(
  new GetVolSizes()
);


enough.check()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => enough.close()
);



`complex` function, the feature provided in v1.x, is discontinued in v2.x.

`collaborate` function is recommended to be used instead. 


The sample code in this tutrial coperates where server1 to 5 could be resolved as a sshable server.


# Collaborate with other servers 

A complicated system faces the case when a relationship of variety of servers is needed to be tested.
Many types of server's state must be compared, summed up, or tested filling mutual exclusive.

In such case, `collaborate` function is effective.

![collaboratewith-other-servers.gif](https://gitlab.com/mjusui/submarine/raw/master/doc/en/Tutrial/09-Collaborate-with-other-servers/Collaborate-with-other-servers.gif?inline=false)

```Collaborate-with-other-servers.js
const Submarine=require('Submarine');


const GetVolSize=class extends Submarine {
  query(){
    return {
      volkb_sizes: String.raw`
        df -P \
          |grep -v "^Filesystem" \
          |awk '{print $3+$4}'
      `,
    };
  }
  format(stats){
    return {
      volkb_largest: stats.volkb_sizes
        .split(/\r\n|\r|\n/)
        .sort(
          (a, b) => b*1 - a*1
        )[0], // largest volume is chosen.
    };
  }

  test(r){
    const { stats }=r;

    return {
      size_enough:
        2 * 1024 * 1024 < stats.volkb_largest,
    };
  }
}

const TestTotalVolSize=class extends Submarine {
  test(r){
    const { collabs }=r;

    return {
      each_size_enough:
        collabs.every(
          collab => collab.exams.ok
        ),
      total_size_enough:
        4 * 1024 *1024 < collabs.reduce((siz, collab)=>{
          return siz + collab.stats.volkb_largest;
        }, 0), // larger than 4MB
    };
  }
}


const GetVolSizes=Submarine.collect(
  host => new GetVolSize({
    conn: 'ssh',
    host: host,
  }),

  { type: 'gen',
    coll: 'bash',
    cmd: 'echo server{1..5}', },
  { type: 'fil',
    coll: 'func',
    func: hosts => hosts.filter(
      host => host.match(/[2-4]$/)
    ), },
  { type: 'fil',
    coll: 'ping', }
);


const enough=new TestTotalVolSize({
  conn: 'sh',
}).collaborate(
  new GetVolSizes()
);


enough.check()
  .then(
    r => JSON.stringify(r)
  ).then(console.log)
  .catch(console.error)
.finally(
  _ => enough.close()
);
```

In this code, `GetVolSize` class is to get volume size from a single server, and `TestTotalVolSize` class is to test Total volume size of 3 servers is larger than 4MB.

`GetVolSizes` class is created by `collect` function and its instance is passed to `collaborate` function of the instance of `TestTotalVolSize` class.

the result from `check` function of the instance passed to `collaborate` function is accessable by name of `collabs` in `query`, `test`, `command` and `batch` function defined in `TestTotalVolSize` class.









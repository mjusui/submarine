# Intro

## About Submarine

Submarine is an Exhaustive State Management Framework for IT Infrastructure. The Concept is very usual to Engineers, that is, all you do is getting what it is, testing if it is what to be, and making it correct if needed.

## Why Submarine

Configuration Management System, like Ansible, Chef and so on, and Test framework, like Serverspec, which have been used to keep states of servers reproductive and error-detectable. These tools, often said "Infrastructure as Code (IaC)", are considerably successful and still survive today Container-based technology (Docker, Kubernetes etc.) has grown.

Submarine.js is intended to cover wider range than these IaC softwares. Following Ansible, ssh is just required to connect servers (agentless). And It operates in Node.js runtime with its Shell executer.

Plus some features are provided.

* Free from Idempotency (a concept in most configuration management system)
* Development with ShellScript templated and ordered by JavaScript (Node.js)
* Other JavaScript (Node.js) feaure is available, such as Class-based extensions
* Separation between Read and Write ops
* Simple Test interface
* Providing HTTP endpoints of JSON-formatted server states
* Providing HTTP endpoints of HTML expressions of server states
* Collaboration among different types of servers

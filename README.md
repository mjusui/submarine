# Submarine

#### Deeply dive into the IT infrastructure

* TODO: README in Japanese

## About

Submarine.js is an Exhaustive State Management Framework for IT Infrastructure. The Concept is very usual to Engineers, that is, all you do is getting what it is, testing if it is what to be, and making it correct if needed.

* More detail  
  https://gitlab.com/mjusui/submarine/tree/latest/doc/en/Intro

## Getting started

** When you start, some programing skill with Node.js may be needed. **

### Prerequisites

* Node.js v12.x is installed (Install with NVM is recommented)
  * Install Node.js with NVM: https://nodejs.org/en/download/package-manager/#nvm
* `bash` and `ssh` command is essentially executable in your environment
  * This repository is now tested only in Ubuntu 18.04 but could operates on the other Distributions.
* Target servers are accessable by ssh with no password

### Install

1. Make your project and `node_modules` directory

```
$ mkdir -p path/to/your/project/node_modules \
  && cd path/to/your/project/node_modules
```

2. Download and unarchive the latest tarball

```
$ curl -LO https://gitlab.com/mjusui/submarine/-/archive/latest/submarine-latest.tar.gz \
  && tar xzf submarine-latest.tar.gz
```

3. Rename directory you prefer to load it

```
$ mv submarine-latest Submarine
```


### Run Hello World

Exec a Node.js program

```
$ node Submarine/src/HelloWorld
{ msg: 'Hello World !' }
```

### Next Step

* Tutrial  
  https://gitlab.com/mjusui/submarine/tree/latest/doc/en/Tutrial

